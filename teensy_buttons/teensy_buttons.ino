////////////////////////////////////////////////////////////
// Teensy-to-RetroShield Adapter Button Example
//
// There are two buttons labeled as "P" and "C".
// Following functions can be used to detect the state
// of the buttons.
//
// 2020/05/01
// Version 0.1
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// These functions return real-time state (no debouncing)
////////////////////////////////////////////////////////////

#define btn_P_state() (analogRead(A21) < 10 ? true : false)
#define btn_C_state() (analogRead(A22) < 10 ? true : false)

////////////////////////////////////////////////////////////
// These functions are debounced.
// Note: They block if button state changes.
////////////////////////////////////////////////////////////
long debounceDelay = 16;

bool btn_P_debounced() 
{
  static bool btn_P_prev_state = false;
  bool btn_P = btn_P_state();

  if ( btn_P != btn_P_prev_state )    // Did button state change?
  {
    delay(debounceDelay);             // If so, wait and recheck
    return (btn_P_prev_state = btn_P_state());
  }
  return btn_P;
}

bool btn_C_debounced() 
{
  static bool btn_C_prev_state = false;
  bool btn_C = btn_C_state();
  
  if ( btn_C != btn_C_prev_state )    // Did button state change?
  {
    delay(debounceDelay);             // If so, wait and recheck
    return (btn_C_prev_state = btn_C_state());
  }
  return btn_C;
}

////////////////////////////////////////////////////////////
// Example code to turn on LED based on P-button
////////////////////////////////////////////////////////////

// Pin 13: Teensy 3.X has the LED on pin 13
int ledPin = 13; 

void setup() {
  // declare the ledPin as an OUTPUT:
  pinMode(ledPin, OUTPUT);  

  Serial.begin(115200);
}

void loop() {
  // read the value from the sensor:
  
  Serial.print("Realtime "); Serial.print(btn_P_state() );     Serial.print(" ");   Serial.println(btn_C_state() );
  Serial.print("Debounced"); Serial.print(btn_P_debounced() ); Serial.print("-");   Serial.println(btn_C_debounced() );

  digitalWrite(ledPin, btn_P_debounced() );

  delay(100);
}
