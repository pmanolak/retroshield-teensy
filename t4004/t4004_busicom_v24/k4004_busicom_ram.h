#ifndef _K4004_BUSICOM_RAM_H
#define _K4004_BUSICOM_RAM_H

////////////////////////////////////////////////////////////////////
// MEMORY LAYOUT
////////////////////////////////////////////////////////////////////

// ##################################################
// MEMORY
#define RAM_BANKS           8     // CM0: 1x RAM0 + CM3CM2CM1: 7x RAM321
#define RAM_CHIPnREG        16    // 4 chips * 4 registers (D3D2 chip select, D1D0 reg select)
#define RAM_CHARS_PER_REG   16    // D3D2D1D0 char select
#define RAM_STAT_PER_REG    4
#define ROM_BANKS           16     

byte  RAM_CHARS[RAM_BANKS][RAM_CHIPnREG]   [RAM_CHARS_PER_REG];
byte RAM_STATUS[RAM_BANKS][RAM_CHIPnREG]   [RAM_STAT_PER_REG];
byte     RAM_IO[RAM_BANKS][RAM_CHIPnREG>>2];
byte     ROM_IO[ROM_BANKS];
byte SRC_ROM_X2[ROM_BANKS];
byte SRC_RAM_X2[RAM_BANKS];   // Saves SRC info for each bank (DCL can change and expect RAM to remember)
byte SRC_RAM_X3[RAM_BANKS];


// ##################################################
// Prep memory/devices before execution start
inline __attribute__((always_inline))
void k4004_IO_init()
{  
  for (unsigned int a=0; a < RAM_BANKS; a++)
    for (unsigned int b=0; b < RAM_CHIPnREG; b++)
        for (unsigned int d=0; d < RAM_CHARS_PER_REG; d++)
          RAM_CHARS[a][b][d] = 0x00;
          
  for (unsigned int a=0; a < RAM_BANKS; a++)
    for (unsigned int b=0; b < RAM_CHIPnREG; b++)
        for (unsigned int e=0; e < RAM_STAT_PER_REG; e++)
            RAM_STATUS[a][b][e] = 0x00;

  for (unsigned int a=0; a < RAM_BANKS; a++)
    for (unsigned int b=0; b < (RAM_CHIPnREG >> 2); b++) 
            RAM_IO[a][b] = 0x00;
  
  for (unsigned int a=0; a < ROM_BANKS; a++)
            ROM_IO[a] = 0x00;
}

#endif