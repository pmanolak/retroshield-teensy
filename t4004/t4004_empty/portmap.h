#ifndef _PORTMAP_H
#define _PORTMAP_H

// This is a complicated header file.
// it defines port macros based on Arduino or Teeny and which
// version of teensy. Fastest port access are done using 
// direct access to ports.


// ##################################################
#if (ARDUINO_AVR_MEGA2560)
// ##################################################
// DIO2 library required on MEGA for fast GPIO access.
#include <avr/pgmspace.h>
#include "pins2_arduino.h"
#include <DIO2.h>

// DIO2 library uses ..2 instead of ..Fast
#define digitalWriteFast(PORT,DATA)  digitalWrite2(PORT,DATA)
#define digitalReadFast(PORT)        digitalRead2(PORT)
#endif

// ##################################################
#if (ARDUINO_AVR_MEGA2560)
// ##################################################

#define MEGA_PD7  (38)
#define MEGA_PG0  (41)
#define MEGA_PG1  (40)
#define MEGA_PG2  (39)
#define MEGA_PB0  (53)
#define MEGA_PB1  (52)
#define MEGA_PB2  (51)
#define MEGA_PB3  (50)

#define MEGA_PC7  (30)
#define MEGA_PC6  (31)
#define MEGA_PC5  (32)
#define MEGA_PC4  (33)
#define MEGA_PC3  (34)
#define MEGA_PC2  (35)
#define MEGA_PC1  (36)
#define MEGA_PC0  (37)

#define MEGA_PL7  (42)
#define MEGA_PL6  (43)
#define MEGA_PL5  (44)
#define MEGA_PL4  (45)
#define MEGA_PL3  (46)
#define MEGA_PL2  (47)
#define MEGA_PL1  (48)
#define MEGA_PL0  (49)

#define MEGA_PA7  (29)
#define MEGA_PA6  (28)
#define MEGA_PA5  (27)
#define MEGA_PA4  (26)
#define MEGA_PA3  (25)
#define MEGA_PA2  (24)
#define MEGA_PA1  (23)
#define MEGA_PA0  (22)

// ##################################################
#elif (ARDUINO_TEENSY35 || ARDUINO_TEENSY36 || ARDUINO_TEENSY41)
// ##################################################

#define MEGA_PD7  (24)
#define MEGA_PG0  (13)
#define MEGA_PG1  (16)
#define MEGA_PG2  (17)
#define MEGA_PB0  (28)
#define MEGA_PB1  (39)
#define MEGA_PB2  (29)
#define MEGA_PB3  (30)

#define MEGA_PC7  (27)
#define MEGA_PC6  (26)
#define MEGA_PC5  (4)
#define MEGA_PC4  (3)
#define MEGA_PC3  (38)
#define MEGA_PC2  (37)
#define MEGA_PC1  (36)
#define MEGA_PC0  (35)

#define MEGA_PL7  (5)
#define MEGA_PL6  (21)
#define MEGA_PL5  (20)
#define MEGA_PL4  (6)
#define MEGA_PL3  (8)
#define MEGA_PL2  (7)
#define MEGA_PL1  (14)
#define MEGA_PL0  (2)

#define MEGA_PA7  (12)
#define MEGA_PA6  (11)
#define MEGA_PA5  (25)
#define MEGA_PA4  (10)
#define MEGA_PA3  (9)
#define MEGA_PA2  (23)
#define MEGA_PA1  (22)
#define MEGA_PA0  (15)

#endif

////////////////////////////////////////////////////////////////////
// Processor Control Pins
////////////////////////////////////////////////////////////////////


#define uP_CMRAM0     MEGA_PA0 // 22
#define uP_CMRAM1     MEGA_PA1 // 23
#define uP_CMRAM2     MEGA_PA2 // 24
#define uP_CMRAM3     MEGA_PA3 // 25
#define uP_CMROM      MEGA_PA4 // 26
#define uP_SYNC       MEGA_PA5 // 27

#define uP_PHI1       MEGA_PG1 // 40
#define uP_PHI2       MEGA_PG0 // 41
#define uP_RESET      MEGA_PB1 // 52
#define uP_TEST       MEGA_PB3 // 50
#define DRIVE_DBUS    MEGA_PD7 // 38

#define RAM_Q3        MEGA_PC7
#define RAM_Q2        MEGA_PC6
#define RAM_Q1        MEGA_PC5
#define RAM_Q0        MEGA_PC4

#define ARDUINO_D3    MEGA_PC3
#define ARDUINO_D2    MEGA_PC2
#define ARDUINO_D1    MEGA_PC1
#define ARDUINO_D0    MEGA_PC0

#define uP_D3         MEGA_PL3
#define uP_D2         MEGA_PL2
#define uP_D1         MEGA_PL1
#define uP_D0         MEGA_PL0


////////////////////////////////////////////////////////////////////
// MACROS
////////////////////////////////////////////////////////////////////

byte ADDR_pinTable[] = {
  // D0..D3
  MEGA_PL0,MEGA_PL1,MEGA_PL2,MEGA_PL3     // ,MEGA_PL4,MEGA_PL5,MEGA_PL6,MEGA_PL7,
};

byte DATAIN_pinTable[] = {
  // D0..D3
  MEGA_PL0,MEGA_PL1,MEGA_PL2,MEGA_PL3     // ,MEGA_PL4,MEGA_PL5,MEGA_PL6,MEGA_PL7
};

byte DATAOUT_pinTable[] = {
  // D0..D3
  MEGA_PC0, MEGA_PC1, MEGA_PC2, MEGA_PC3
};

void configure_PINMODE_ADDR()
{
  for (unsigned int i=0; i<sizeof(ADDR_pinTable); i++)
  {
    pinMode(ADDR_pinTable[i], INPUT);
  } 
}

void configure_PINMODE_DATA()
{
  // Port used by Arduino to read DATA from 4004
  for (unsigned int i=0; i<sizeof(DATAIN_pinTable); i++)
  {
    pinMode(DATAIN_pinTable[i], INPUT);
  } 

  // Port used by Arduino to write DATA to 4004
  for (unsigned int i=0; i<sizeof(DATAOUT_pinTable); i++)
  {
    pinMode(DATAOUT_pinTable[i], OUTPUT);
  } 
}

// ##################################################
#if (ARDUINO_AVR_MEGA2560)
// ##################################################

// #define PHI_SET(C)        (PORTG = (PORTG & 0b11111100) | (C & 0b00000011) )
#define PHI2_HIGH         (PORTG = PORTG | 0b00000001)
#define PHI2_LOW          (PORTG = PORTG & 0b11111110)
#define PHI1_HIGH         (PORTG = PORTG | 0b00000010)
#define PHI1_LOW          (PORTG = PORTG & 0b11111101)
#define RESET_HIGH        (PORTB = PORTB | 0b00000010)
#define RESET_LOW         (PORTB = PORTB & 0b11111101)
#define TEST_HIGH         (PORTB = PORTB | 0b00001000)
#define TEST_LOW          (PORTB = PORTB & 0b11110111)
#define DRIVE_DBUS_START  (PORTD = PORTD | 0b10000000)
#define DRIVE_DBUS_STOP   (PORTD = PORTD & 0b01111111)

#define CLK1_HIGH()      PHI1_HIGH // digitalWriteFast(uP_PHI1, HIGH)
#define CLK1_LOW()       PHI1_LOW  // digitalWriteFast(uP_PHI1, LOW)
#define CLK2_HIGH()      PHI2_HIGH // digitalWriteFast(uP_PHI2, HIGH)
#define CLK2_LOW()       PHI2_LOW // digitalWriteFast(uP_PHI2, LOW)

/* Digital Pin Assignments */
/* Remember - 4004 logic is inverted.  1 means 0, and 0 means 1 */
#define DATA4_OUT(D)      ( PORTC = (PORTC & 0xF0) | (~(D) & 0x0F) )
#define DATA4_IN()        ( (~PINL) & 0x0F)
#define STATE_SYNC        ( (~PINA) & 0x20)
#define STATE_CMROM       (((~PINA) & 0x10) >> 4)
#define STATE_CMRAM       ( (~PINA) & 0x0F)
#define STATE_CMRAM_0321  (STATE_CMRAM >> 1) 
// Swizzle CMRAM0 and CMRAM1/2/3
// so table in datasheet maps to 0,1,2,3,4,5,6,7
//- #define STATE_RAMQ        (((~PINC) & 0xF0) >> 4)
#define RAMQ4_OUT(D)      ( PORTC = (PORTC & 0x0F) | (((D) & 0x0F)<< 4) )

inline __attribute__((always_inline))
bool DATA4_OUT_matches_IN()
{
  while (digitalReadFast(ARDUINO_D3) != digitalReadFast(MEGA_PL3));
  while (digitalReadFast(ARDUINO_D2) != digitalReadFast(MEGA_PL2));
  while (digitalReadFast(ARDUINO_D1) != digitalReadFast(MEGA_PL1));
  while (digitalReadFast(ARDUINO_D0) != digitalReadFast(MEGA_PL0));

  return true;
}

// ##################################################
#elif (ARDUINO_TEENSY35 || ARDUINO_TEENSY36)
// ##################################################

#define PHI2_HIGH         digitalWriteFast(uP_PHI2, HIGH)       
#define PHI2_LOW          digitalWriteFast(uP_PHI2, LOW)        
#define PHI1_HIGH         digitalWriteFast(uP_PHI1, HIGH)       
#define PHI1_LOW          digitalWriteFast(uP_PHI1, LOW)       
#define RESET_HIGH        digitalWriteFast(uP_RESET, HIGH)      
#define RESET_LOW         digitalWriteFast(uP_RESET, LOW)       
#define TEST_HIGH         digitalWriteFast(uP_TEST, HIGH)       
#define TEST_LOW          digitalWriteFast(uP_TEST, LOW)        
#define DRIVE_DBUS_START  digitalWriteFast(DRIVE_DBUS, HIGH)  
#define DRIVE_DBUS_STOP   digitalWriteFast(DRIVE_DBUS, LOW)    

// #define CLK1_HIGH()      {wait_for_next_edge(); PHI1_HIGH;} // digitalWriteFast(uP_PHI1, HIGH)
// #define CLK1_LOW()       {wait_for_next_edge(); PHI1_LOW;}  // digitalWriteFast(uP_PHI1, LOW)
// #define CLK2_HIGH()      {wait_for_next_edge(); PHI2_HIGH;} // digitalWriteFast(uP_PHI2, HIGH)
// #define CLK2_LOW()       {wait_for_next_edge(); PHI2_LOW;}  // digitalWriteFast(uP_PHI2, LOW)

#define CLK1_LOW()       {wait_for_edge_abs(NEXT_EDGE_D2);      PHI1_LOW;}  // digitalWriteFast(uP_PHI1, LOW)
#define CLK1_HIGH()      {wait_for_edge_abs(NEXT_EDGE_CLK1_W);  PHI1_HIGH;} // digitalWriteFast(uP_PHI1, HIGH)
#define CLK2_LOW()       {wait_for_edge_abs(NEXT_EDGE_D1);      PHI2_LOW;}  // digitalWriteFast(uP_PHI2, LOW)
#define CLK2_HIGH()      {wait_for_edge_abs(NEXT_EDGE_CLK2_W);  PHI2_HIGH;} // digitalWriteFast(uP_PHI2, HIGH)

/* Digital Pin Assignments */
/* Remember - 4004 logic is inverted.  1 means 0, and 0 means 1 */
#define DATA4_OUT(D)      (GPIOC_PDOR = ( (GPIOC_PDOR & 0xFFFFF0FF) | ( ((~D) & 0b1111)<<8) ))
#define DATA4_IN()        ((byte) (((~ ((byte) GPIOD_PDIR) ) & 0b00001111)) )
#define STATE_SYNC        (!digitalReadFast(uP_SYNC))         
#define STATE_CMROM       (!digitalReadFast(uP_CMROM))   
#define STATE_CMRAM       ((byte) ((( ((byte) (~GPIOC_PDIR)) ) & 0b00001111)) )
#define STATE_CMRAM_0321  (STATE_CMRAM >> 1) 
// Swizzle CMRAM0 and CMRAM1/2/3
// so table in datasheet maps to 0,1,2,3,4,5,6,7
#define RAMQ4_OUT(D)      (GPIOA_PDOR = ( (GPIOA_PDOR & 0xFFFF0FFF) | ( (D & 0b1111)<<12) ))

inline __attribute__((always_inline))
bool DATA4_OUT_matches_IN()
{
  while (digitalReadFast(ARDUINO_D3) != digitalReadFast(MEGA_PL3));
  while (digitalReadFast(ARDUINO_D2) != digitalReadFast(MEGA_PL2));
  while (digitalReadFast(ARDUINO_D1) != digitalReadFast(MEGA_PL1));
  while (digitalReadFast(ARDUINO_D0) != digitalReadFast(MEGA_PL0));

  return true;
}

// ##################################################
#elif (ARDUINO_TEENSY41)
// ##################################################

// Teensy 4.1 has different port/pin assignments compared to Teeny 3.5 & 3.6
// so we have to do bit shuffling to construct address and data buses.
// Teensy 4.1's 600Mhz seems to compensate by executing rest faster.

#define PHI2_HIGH         digitalWriteFast(uP_PHI2, HIGH) 
#define PHI2_LOW          digitalWriteFast(uP_PHI2, LOW)  
#define PHI1_HIGH         digitalWriteFast(uP_PHI1, HIGH)  
#define PHI1_LOW          digitalWriteFast(uP_PHI1, LOW)   
#define RESET_HIGH        digitalWriteFast(uP_RESET, HIGH)   
#define RESET_LOW         digitalWriteFast(uP_RESET, LOW) 
#define TEST_HIGH         digitalWriteFast(uP_TEST, HIGH)  
#define TEST_LOW          digitalWriteFast(uP_TEST, LOW)  
#define DRIVE_DBUS_START  digitalWriteFast(DRIVE_DBUS, HIGH)   
#define DRIVE_DBUS_STOP   digitalWriteFast(DRIVE_DBUS, LOW)  

// Symmetric Clock
// #define CLK1_HIGH()      {wait_for_next_edge(); PHI1_HIGH;} // digitalWriteFast(uP_PHI1, HIGH)
// #define CLK1_LOW()       {wait_for_next_edge(); PHI1_LOW;}  // digitalWriteFast(uP_PHI1, LOW)
// #define CLK2_HIGH()      {wait_for_next_edge(); PHI2_HIGH;} // digitalWriteFast(uP_PHI2, HIGH)
// #define CLK2_LOW()       {wait_for_next_edge(); PHI2_LOW;}  // digitalWriteFast(uP_PHI2, LOW)

#define CLK1_LOW()       {wait_for_edge_abs(NEXT_EDGE_D2);      PHI1_LOW;}  // digitalWriteFast(uP_PHI1, LOW)
#define CLK1_HIGH()      {wait_for_edge_abs(NEXT_EDGE_CLK1_W);  PHI1_HIGH;} // digitalWriteFast(uP_PHI1, HIGH)
#define CLK2_LOW()       {wait_for_edge_abs(NEXT_EDGE_D1);      PHI2_LOW;}  // digitalWriteFast(uP_PHI2, LOW)
#define CLK2_HIGH()      {wait_for_edge_abs(NEXT_EDGE_CLK2_W);  PHI2_HIGH;} // digitalWriteFast(uP_PHI2, HIGH)

/* Digital Pin Assignments */
/* Remember - 4004 logic is inverted.  1 means 0, and 0 means 1 */
//- #define DATA4_OUT(D)      ( PORTC = (PORTC & 0xF0) | (~(D) & 0x0F) )
//- #define DATA4_IN()        ( (~PINL) & 0x0F)
#define STATE_SYNC        (!digitalReadFast(uP_SYNC))   // ( (~PINA) & 0x20)
#define STATE_CMROM       (!digitalReadFast(uP_CMROM))  // (((~PINA) & 0x10) >> 4)
#define STATE_CMRAM       (STATE_CMRAM_TEENSY())        // ( (~PINA) & 0x0F)
#define STATE_CMRAM_0321  (STATE_CMRAM_TEENSY() >> 1)   // (STATE_CMRAM >> 1) 
// Swizzle CMRAM0 and CMRAM1/2/3
// so table in datasheet maps to 0,1,2,3,4,5,6,7
//- #define STATE_RAMQ        (((~PINC) & 0xF0) >> 4)
// #define RAMQ4_OUT(D)      ( PORTC = (PORTC & 0x0F) | (((D) & 0x0F)<< 4) )

inline __attribute__((always_inline))
byte STATE_CMRAM_TEENSY()
{
  byte b = 0;

  b = b | digitalReadFast(MEGA_PA3);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PA2);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PA1);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PA0);

  return ((~b) & 0x0F);    // 4004 is inverse-logic
}

inline __attribute__((always_inline))
void RAMQ4_OUT(byte b)
{
  // LED's don't work on Teensy due to level shifters.

  digitalWriteFast(RAM_Q0, (b & 1));
  b = b >> 1;
  // digitalWriteFast(RAM_Q1, (b & 1));
  b = b >> 1;
  // digitalWriteFast(RAM_Q2, (b & 1));
  b = b >> 1;
  digitalWriteFast(RAM_Q3, (b & 1));
}

inline __attribute__((always_inline))
void DATA4_OUT(byte b)
{
  b = ~(b);   // 4004 is inverse-logic.

  digitalWriteFast(ARDUINO_D0, (b & 1));
  b = b >> 1;
  digitalWriteFast(ARDUINO_D1, (b & 1));
  b = b >> 1;
  digitalWriteFast(ARDUINO_D2, (b & 1));
  b = b >> 1;
  digitalWriteFast(ARDUINO_D3, (b & 1));
}

inline __attribute__((always_inline))
bool DATA4_OUT_matches_IN()
{
  while (digitalReadFast(ARDUINO_D3) != digitalReadFast(MEGA_PL3));
  while (digitalReadFast(ARDUINO_D2) != digitalReadFast(MEGA_PL2));
  while (digitalReadFast(ARDUINO_D1) != digitalReadFast(MEGA_PL1));
  while (digitalReadFast(ARDUINO_D0) != digitalReadFast(MEGA_PL0));

  return true;
}

inline __attribute__((always_inline))
byte DATA4_IN()
{
  byte b = 0;

  b = b | digitalReadFast(MEGA_PL3);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PL2);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PL1);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PL0);

  return ((~b) & 0x0F);    // 4004 is inverse-logic
}


// ##################################################
#endif
// ##################################################

void print_teensy_version()
{
#if (ARDUINO_AVR_MEGA2560)
  Serial.println("Arduino:    Mega2560");
#elif (ARDUINO_TEENSY35)
  Serial.println("Teensy:     3.5");
#elif (ARDUINO_TEENSY36)
  Serial.println("Teensy:     3.6");
#elif (ARDUINO_TEENSY41)
  Serial.println("Teensy:     4.1");
#endif
}

#endif    // _PORTMAP_H



// Reference 

// Teensy 3.5/3.6
//
// #define GPIO?_PDOR    (*(volatile uint32_t *)0x400FF0C0) // Port Data Output Register
// #define GPIO?_PSOR    (*(volatile uint32_t *)0x400FF0C4) // Port Set Output Register
// #define GPIO?_PCOR    (*(volatile uint32_t *)0x400FF0C8) // Port Clear Output Register
// #define GPIO?_PTOR    (*(volatile uint32_t *)0x400FF0CC) // Port Toggle Output Register
// #define GPIO?_PDIR    (*(volatile uint32_t *)0x400FF0D0) // Port Data Input Register
// #define GPIO?_PDDR    (*(volatile uint32_t *)0x400FF0D4) // Port Data Direction Register
//

// TEENSY4.1 
//
// CORE_PIN28_BIT
// CORE_PIN28_BITMASK
// CORE_PIN28_PORTREG
// CORE_PIN28_PORTSET
// CORE_PIN28_PORTCLEAR
// CORE_PIN28_PORTTOGGLE
// CORE_PIN28_DDRREG	(1=OUT, 0=IN)
// CORE_PIN28_PINREG

