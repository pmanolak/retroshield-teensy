#ifndef _K4004_BUSICOM_ROM_H
#define _K4004_BUSICOM_ROM_H

// ROM(s) (Monitor)
#define ROM_START   ((word) 0x0000)
#define ROM_END     ((word) (ROM_START+sizeof(rom_bin)-1))
#define rom_bin     rom_bin_DEBUG

PROGMEM const unsigned char rom_bin_DEBUG[] = {
// 0000: START
// 0000:        NOP             00          ;; Recommended by datasheet.
// 0001:        FIM P0 $A2      20 A2
// 0003:        LD  R0          A0
// 0004:        ADD R1          81
// 0005:        XCH R1          B1
// 0006: DONE   
// 0006:        JUN START       40 00
  
  0x00, 0x20, 0xA2, 0xA0, 0x81, 0xB1, 0x40, 0x00
};

// Disassembler output
// 
// 000: 00 NOP
// 001: 20 FIM     ; Fetch immediate from ROM into reg pair 0_1
// 002: A2         ; ^^^^^^^^ ^^^^^^^^
// 003: A0 LD      ; load registter 0 into accumulator
// 004: 81 ADD CY   ; add register 1 and carry to accumulator
// 005: B1 XCH     ; exchange registter 1 with accumulator
// 006: 40 JUN     ; jump unconditional
// 007: 00         ; ^^^^^^^^ ^^^^^^^^

#endif
