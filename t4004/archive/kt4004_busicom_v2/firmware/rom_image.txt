Thanks to ..TODO... 
for helping extract the ROM images.

Busicom ROM image was downloaded from:

https://www.4004.com/assets/busicom-141pf-simulator-w-flowchart-071113.zip

File "4001.code":
============================================================
busicode - Verilog memory list,
created from ..\asm\busicode.hex by hex2code.pl,
  StarBoard Design,
============================================================

(https://4004.com/busicom-simulator-documentation.html)

// Huge THANKS to following people for extracting rare ROM images
// and making them available for public.  Now we can have history
// running.

// Team Personnel (in alphabetical order)
//   Allen E. Armstrong, mechanical engineering
//   Fred Huettig, netlist extraction software, electrical engineering, FPGA programming
//   Lajos Kintli, reverse-engineering, software analysis, commenting, documentation
//   Tim McNerney, conceptual design, project leader, digital design
//   Barry Silverman, reverse-engineering, simulation software
//   Brian Silverman, reverse-engineering, simulation software
// Collaborators and advisors
//   Federico Faggin, CEO Foveon, 4004 chip designer
//   Sellam Ismail, proprietor, Vintage Tech
//   Tom Knight, MIT CSAIL, VLSI history expert
//   Tracey Mazur, Curator, Intel Museum
//   Dag Spicer, Curator, Computer History Museum
//   Rachel Stewart, former Intel archivist
// Other contributors
//   Christian Bassow, owner of a Busicom 141-PF, who kindly sent me photographs of the PCB.
//   Karen Joyce, electronic assembler extraordinaire
//   Matt McNerney, webmaster

