#ifndef _SETUPHOLD_H
#define _SETUPHOLD_H

// ##################################################
// Adjust setup/hold times based on board
// Arduino Mega 2560  = 16Mhz   (0x)
// Teensy 3.5         = 120Mhz  (1x)
// Teensy 3.6         = 180Mhz  (1.5x)
// Teensy 4.1         = 600Mhz  (5x)
// ##################################################

// 
// DELAY_MEGA_TEENSY_NS(MEGA,TEENSY) macro will pick
// different delays for Arduino vs Teensy.
//
#if (ARDUINO_AVR_MEGA2560)
  #define DELAY_MEGA_TEENSY_NS(MEGA,TEENSY)  DELAY_UNIT_##MEGA##NS()
#elif ( (ARDUINO_TEENSY35) || (ARDUINO_TEENSY36) || (ARDUINO_TEENSY41) )
  #define DELAY_MEGA_TEENSY_NS(MEGA,TEENSY)  DELAY_UNIT_##TEENSY##NS()
#endif

// ##################################################
#if (ARDUINO_AVR_MEGA2560)
// ##################################################
  
  #define DELAY_UNIT()          asm volatile("nop\n")
  #define DELAY_FACTOR_H()      {DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT();} // DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); }
  #define DELAY_FACTOR_L()      {DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT();} // DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); }

  // FYI: These won't be  exact because the digitalWriteFast() also has delays.
  #define DELAY_UNIT_0NS()          asm volatile("")
  #define DELAY_UNIT_60NS()         asm volatile("nop\n")
  #define DELAY_UNIT_120NS()        asm volatile("nop\nnop\n")
  #define DELAY_UNIT_180NS()        asm volatile("nop\nnop\nnop\n")
  #define DELAY_UNIT_240NS()        asm volatile("nop\nnop\nnop\nnop\n")
  #define DELAY_UNIT_300NS()        asm volatile("nop\nnop\nnop\nnop\nnop\n")
  #define DELAY_UNIT_360NS()        asm volatile("nop\nnop\nnop\nnop\nnop\nnop\n")
  #define DELAY_UNIT_420NS()        asm volatile("nop\nnop\nnop\nnop\nnop\nnop\nnop\n")
  //- #define DELAY_FOR_BUFFER_60NS()   {DELAY_UNIT_60NS();}    // N/A for Mega (Delay for level shifters (teensy) to pass data out)

// ##################################################
#elif (ARDUINO_TEENSY35)
// ##################################################

  #define DELAY_UNIT_25NS()         asm volatile("nop\nnop\nnop\n")

  #define DELAY_UNIT_100NS()        {DELAY_UNIT_25NS();DELAY_UNIT_25NS();DELAY_UNIT_25NS();DELAY_UNIT_25NS();}
  #define DELAY_UNIT_125NS()        {DELAY_UNIT_25NS();DELAY_UNIT_25NS();DELAY_UNIT_25NS();DELAY_UNIT_25NS();DELAY_UNIT_25NS();}  
  #define DELAY_UNIT_50NS()         {DELAY_UNIT_25NS();DELAY_UNIT_25NS();}

  // FYI: These won't be  exact because the digitalWriteFast() also has delays.
  //                                Add 25ns as buffer
  #define DELAY_UNIT_0NS()          {}
  #define DELAY_UNIT_60NS()         {DELAY_UNIT_50NS();  DELAY_UNIT_25NS();DELAY_UNIT_25NS();} // asm volatile("nop\n")
  #define DELAY_UNIT_120NS()        {DELAY_UNIT_50NS();  DELAY_UNIT_100NS();DELAY_UNIT_25NS();} // asm volatile("nop\nnop\n")
  #define DELAY_UNIT_180NS()        {DELAY_UNIT_50NS();  DELAY_UNIT_100NS();DELAY_UNIT_25NS();DELAY_UNIT_25NS();DELAY_UNIT_25NS();} // asm volatile("nop\nnop\nnop\n")
  #define DELAY_UNIT_240NS()        {DELAY_UNIT_50NS();  DELAY_UNIT_100NS();DELAY_UNIT_100NS();DELAY_UNIT_25NS();DELAY_UNIT_25NS();} // asm volatile("nop\nnop\nnop\nnop\n")
  #define DELAY_UNIT_300NS()        {DELAY_UNIT_50NS();  DELAY_UNIT_100NS();DELAY_UNIT_100NS();DELAY_UNIT_100NS();} // asm volatile("nop\nnop\nnop\nnop\nnop\n")
  #define DELAY_UNIT_360NS()        {DELAY_UNIT_50NS();  DELAY_UNIT_100NS();DELAY_UNIT_100NS();DELAY_UNIT_100NS();DELAY_UNIT_50NS();} // asm volatile("nop\nnop\nnop\nnop\nnop\nnop\n")
  #define DELAY_UNIT_420NS()        {DELAY_UNIT_50NS();  DELAY_UNIT_100NS();DELAY_UNIT_100NS();DELAY_UNIT_100NS();DELAY_UNIT_100NS();DELAY_UNIT_25NS();} // asm volatile("nop\nnop\nnop\nnop\nnop\nnop\nnop\n")


// ##################################################
#elif (ARDUINO_TEENSY36)
// ##################################################

  #define DELAY_UNIT_25NS()         asm volatile("nop\nnop\nnop\nnop\n")    // 22-23ns

  #define DELAY_UNIT_100NS()        {DELAY_UNIT_25NS();DELAY_UNIT_25NS();DELAY_UNIT_25NS();DELAY_UNIT_25NS();}
  #define DELAY_UNIT_125NS()        {DELAY_UNIT_25NS();DELAY_UNIT_25NS();DELAY_UNIT_25NS();DELAY_UNIT_25NS();DELAY_UNIT_25NS();}
  #define DELAY_UNIT_50NS()         {DELAY_UNIT_25NS();DELAY_UNIT_25NS();}

  // FYI: These won't be  exact because the digitalWriteFast() also has delays.
  //                                Add 25ns as buffer
  #define DELAY_UNIT_0NS()          {}
  #define DELAY_UNIT_60NS()         {DELAY_UNIT_125NS();  DELAY_UNIT_25NS();DELAY_UNIT_25NS();} // asm volatile("nop\n")
  #define DELAY_UNIT_120NS()        {DELAY_UNIT_125NS();  DELAY_UNIT_100NS();DELAY_UNIT_25NS();} // asm volatile("nop\nnop\n")
  #define DELAY_UNIT_180NS()        {DELAY_UNIT_125NS();  DELAY_UNIT_100NS();DELAY_UNIT_25NS();DELAY_UNIT_25NS();DELAY_UNIT_25NS();} // asm volatile("nop\nnop\nnop\n")
  #define DELAY_UNIT_240NS()        {DELAY_UNIT_125NS();  DELAY_UNIT_100NS();DELAY_UNIT_100NS();DELAY_UNIT_25NS();DELAY_UNIT_25NS();} // asm volatile("nop\nnop\nnop\nnop\n")
  #define DELAY_UNIT_300NS()        {DELAY_UNIT_125NS();  DELAY_UNIT_100NS();DELAY_UNIT_100NS();DELAY_UNIT_100NS();} // asm volatile("nop\nnop\nnop\nnop\nnop\n")
  #define DELAY_UNIT_360NS()        {DELAY_UNIT_125NS();  DELAY_UNIT_100NS();DELAY_UNIT_100NS();DELAY_UNIT_100NS();DELAY_UNIT_50NS();} // asm volatile("nop\nnop\nnop\nnop\nnop\nnop\n")
  #define DELAY_UNIT_420NS()        {DELAY_UNIT_125NS();  DELAY_UNIT_100NS();DELAY_UNIT_100NS();DELAY_UNIT_100NS();DELAY_UNIT_100NS();DELAY_UNIT_25NS();} // asm volatile("nop\nnop\nnop\nnop\nnop\nnop\nnop\n")


// ##################################################
#elif (ARDUINO_TEENSY41)
// ##################################################

  #define DELAY_UNIT_25NS() asm volatile("nop\nnop\nnop\nnop\nnop\nnop\n" \
                                         "nop\nnop\nnop\nnop\nnop\nnop\n" \
                                         "nop\nnop\nnop\nnop\nnop\nnop\n" \
                                         "nop\nnop\nnop\n" \
                                         "nop\nnop\nnop\nnop\n" \
                                         "nop\nnop\nnop\nnop\nnop\nnop\n")

  #define DELAY_UNIT_100NS()        {DELAY_UNIT_25NS();DELAY_UNIT_25NS();DELAY_UNIT_25NS();DELAY_UNIT_25NS();}
  #define DELAY_UNIT_125NS()        {DELAY_UNIT_25NS();DELAY_UNIT_25NS();DELAY_UNIT_25NS();DELAY_UNIT_25NS();DELAY_UNIT_25NS();}
  #define DELAY_UNIT_50NS()         {DELAY_UNIT_25NS();DELAY_UNIT_25NS();}

  // FYI: These won't be  exact because the digitalWriteFast() also has delays.
  //                                Add 25ns as buffer
  #define DELAY_UNIT_0NS()          {}
  #define DELAY_UNIT_60NS()         {DELAY_UNIT_100NS();  DELAY_UNIT_25NS();DELAY_UNIT_25NS();} // asm volatile("nop\n")
  #define DELAY_UNIT_120NS()        {DELAY_UNIT_100NS();  DELAY_UNIT_100NS();DELAY_UNIT_25NS();} // asm volatile("nop\nnop\n")
  #define DELAY_UNIT_180NS()        {DELAY_UNIT_100NS();  DELAY_UNIT_100NS();DELAY_UNIT_25NS();DELAY_UNIT_25NS();DELAY_UNIT_25NS();} // asm volatile("nop\nnop\nnop\n")
  #define DELAY_UNIT_240NS()        {DELAY_UNIT_100NS();  DELAY_UNIT_100NS();DELAY_UNIT_100NS();DELAY_UNIT_25NS();DELAY_UNIT_25NS();} // asm volatile("nop\nnop\nnop\nnop\n")
  #define DELAY_UNIT_300NS()        {DELAY_UNIT_100NS();  DELAY_UNIT_100NS();DELAY_UNIT_100NS();DELAY_UNIT_100NS();} // asm volatile("nop\nnop\nnop\nnop\nnop\n")
  #define DELAY_UNIT_360NS()        {DELAY_UNIT_100NS();  DELAY_UNIT_100NS();DELAY_UNIT_100NS();DELAY_UNIT_100NS();DELAY_UNIT_50NS();} // asm volatile("nop\nnop\nnop\nnop\nnop\nnop\n")
  #define DELAY_UNIT_420NS()        {DELAY_UNIT_100NS();  DELAY_UNIT_100NS();DELAY_UNIT_100NS();DELAY_UNIT_100NS();DELAY_UNIT_100NS();DELAY_UNIT_25NS();} // asm volatile("nop\nnop\nnop\nnop\nnop\nnop\nnop\n")

#endif

#endif  // _SETUPHOLD_H