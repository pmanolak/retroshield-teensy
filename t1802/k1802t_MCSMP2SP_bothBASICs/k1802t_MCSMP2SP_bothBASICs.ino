////////////////////////////////////////////////////////////////////
// RetroShield 1802
// 2020/01/12
// Version 1.0
// Erturk Kocalar
//
// The MIT License (MIT)
//
// Copyright (c) 2019 Erturk Kocalar, 8Bitforce.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
// Date         Comments                                            Author
// -----------------------------------------------------------------------------
// 01/12/2020   Initial Release (MCSMP + BASIC3 + TinyBasic).       E. Kocalar
// 04/04/2024   Add teensy 3.5 & 4.1 support                        E. Kocalar 
//
//
//  WARNING:
//    1802 is a completely different beast than other micros I've played with.
//    It supports I/O as well as DMA. Hence the databus accesses are more 
//    involved than a simple 6502. I added enough support to run BASIC 
//    and monitor similar to Lee Hart's Membership Card.  As I learn more
//    about this processor, we can add more devices for I/O and DMA.
//

////////////////////////////////////////////////////////////////////
// Options
//   outputDEBUG: Print memory access debugging messages.
//   TPB_SOFTWARE: Generate TPB internally so access SC0 signal. 
////////////////////////////////////////////////////////////////////
#define outputDEBUG     0
#define TPB_SOFTWARE    1

////////////////////////////////////////////////////////////////////
// 1802 DEFINITIONS
////////////////////////////////////////////////////////////////////

#include "setuphold.h"      // Delays required to meet setup/hold
#include "portmap.h"        // Pin mapping to cpu
#include "memorymap.h"      // Memory Map (ROM, RAM, PERIPHERALS)


unsigned long clock_cycle_count;

// Address and data are latched onto these variables.
word uP_ADDR;
byte uP_ADDR_H;
byte uP_ADDR_L;
byte uP_DATA;

void uP_init()
{
  // Set directions for ADDR & DATA Bus.
  configure_PINMODE_ADDR();
  configure_PINMODE_DATA();


  pinMode(uP_CLEAR_N,   OUTPUT);
  pinMode(uP_TPA,       INPUT);
  pinMode(uP_TPB,       INPUT);
  pinMode(uP_MRD_N,     INPUT);
  pinMode(uP_MWR_N,     INPUT);

  pinMode(uP_N0,        INPUT);
  pinMode(uP_N1,        INPUT);
  pinMode(uP_N2,        INPUT);
  pinMode(uP_SC0,       INPUT);
  pinMode(uP_SC1,       INPUT);
  pinMode(uP_Q,         INPUT);

  pinMode(uP_EF3_N,     OUTPUT);
  pinMode(uP_EF4_N,     OUTPUT);
  pinMode(uP_INT_N,     OUTPUT);
  pinMode(uP_DMAIN_N,   OUTPUT);
  pinMode(uP_DMAOUT_N,  OUTPUT);
  
  pinMode(uP_CLK,       OUTPUT);

  uP_assert_reset();
  digitalWrite(uP_CLK,  LOW);
  
  clock_cycle_count = 0;
}

void uP_assert_reset()
{
  // Drive RESET conditions
  digitalWrite(uP_CLEAR_N,    LOW);
  digitalWrite(uP_EF3_N,      HIGH);
  digitalWrite(uP_EF4_N,      HIGH);
  digitalWrite(uP_INT_N,      HIGH);
  digitalWrite(uP_DMAIN_N,    HIGH);
  digitalWrite(uP_DMAOUT_N,   HIGH);
  
}
  
void uP_release_reset()
{
  // Drive RESET conditions
  digitalWrite(uP_CLEAR_N, HIGH);
}


////////////////////////////////////////////////////////////////////
// Processor Control Loop
////////////////////////////////////////////////////////////////////
// This is where the action is.
// it reads processor control signals and acts accordingly.
//
// 1802 multiplexed AD15..8/AD7..0 Bus
// TPA=H->L         -> latch A0..A7
// IO_M = LOW       -> Low: Memory, High: IO access
// RD_N = LOW       -> Enable RAM data output
// WR_N = LOW       -> Enable RAM data input

// These signals are read inside the cpu_tick for speed
// because they are used multiple times.
//
word ADDR_latched     = 0;
byte DATA_latched     = 0;
bool prevMRD_N        = 1;
bool prevMWR_N        = 1;
bool STATE_TPA        = 0;
bool STATE_TPB        = 0;
bool STATE_TPB_HW     = 0; // Hardware TPB on older boards
bool STATE_MRD_N      = 1;
bool STATE_MWR_N      = 1;
bool STATE_MRD        = 0;    // Inverted version of STATE_MRD_N;
bool STATE_MWR        = 0;    // Inverted version of STATE_MWR_N
byte STATE_N210       = 0;

// Calculate TPB inside Arduino to possibly save a pin for SC0. see schematics.
byte TPB_cntr         = 6;    // Start at 6 so TPB not asserted during power-up.
                              // count cycles for TPB instead of using a GPIO.
                              // see schematics for hardware backup if TPB
                              // is really needed.
                              
volatile byte DATA_OUT;
volatile byte DATA_IN;

inline __attribute__((always_inline))
void cpu_tick()
{   
  CLK_HIGH;
  DELAY_FACTOR_H();       // delayMicroseconds(1);

  STATE_TPA       = digitalRead(uP_TPA);    // ( (bool) (GPIOC_PDIR & (1 << 11)) );
  STATE_TPB_HW    = digitalRead(uP_TPB);    // ( (bool) (GPIOB_PDIR & (1 << 01)) );   // <=== TPB signal from 1802 is not really needed.
  STATE_MRD_N     = digitalRead(uP_MRD_N);  // ( (bool) (GPIOA_PDIR & (1 << 13)) );   //      because cpu_tick() will generate it internally.
  STATE_MWR_N     = digitalRead(uP_MWR_N);  // ( (bool) (GPIOA_PDIR & (1 << 12)) );   // 
  STATE_N210      = (digitalReadFast(uP_N2) << 2) | (digitalReadFast(uP_N1) << 1) | (digitalReadFast(uP_N0) << 0);
  // STATE_N210      = ( (byte) ( (GPIOC_PDIR & 0b0000011100000000) >> 8) );

#if outputDEBUG
  delay(100);
  Serial.write('#');
#endif


  // Generate TPB Internally.
  //    
#if (TPB_SOFTWARE)
  // To support DMA, we have to generate TPB manually :(
  if (STATE_TPA)
    // Start counting from TPA pulse
    TPB_cntr = 0;
  else
  {
    TPB_cntr++;    
    if (TPB_cntr == 5)
      STATE_TPB = true;
    else
      STATE_TPB = false;
  }

    // Serial.print(STATE_TPB_HW, BIN);Serial.println(STATE_TPB, BIN);
  // Doublecheck HW version and SW version agree: 
  if ( STATE_TPB_HW != STATE_TPB )
  {
    // Serial.print(STATE_TPB_HW, BIN);Serial.println(STATE_TPB, BIN);
    Serial.print("\n!!! TPB Mismatch: "); Serial.println(TPB_cntr, DEC);
    Serial.println("Please report to erturkk@8bitforce.com");
  }
#else
  STATE_TPB = STATE_TPB_HW;
#endif
    
  ////////////////////////////////////////////////////////////
  // ALE
  ////////////////////////////////////////////////////////////
  if (STATE_TPA)      // need to capture address bits when ALE is high.
  {
    uP_ADDR_H = ADDR_H();
    
#if outputDEBUG
    char tmp[40];
    sprintf(tmp, "TPA A=%0.4X MRD=%0.2X/%0.2X MWR=%0.2X/%0.2X\n", ADDR_H, STATE_MRD_N, prevMRD_N, STATE_MWR_N, prevMWR_N);
    // Serial.write(tmp);
#endif
  } 

  //
  // 1802 Input/Output is complicated but cool. Basically, processor
  // just facilitates a memory read/write to/from IO device. So both
  // N210 and MRD#/MWR# are active at the same time.
  // If we are doing IN (IO device data is written to memory), then
  //   we will start driving databus and memory section will take care
  //   memory write (MWR_N toggled).
  // If we are doing OUT (memory data written to IO device), then
  //   memory section will drive databus as expected, but then the IO
  //   section later on will capture the databus on TPB pulse.
  //
  //////////////////////////////////////////////////////////////////////
  // IO Access for INPUT (N2/N1/N0 is 1,2,3,4,5,6,7)
  // which means IO device output (databus) will be written to the memory
  // by MWR# pulse.
  //////////////////////////////////////////////////////////////////////

  if (STATE_N210 && STATE_MRD_N && !STATE_MWR_N && prevMWR_N)
  {    
    // change DATA port to output to uP:
    xDATA_DIR_OUT();

    // figure out what to output based on N210 address.
    DATA_latched = 0x55;

    // start driving DATABus
    DATA_OUT = DATA_latched;
    SET_DATA_OUT( DATA_OUT );
    DELAY_FOR_BUFFER();

#if outputDEBUG
    char tmp[50];
    sprintf(tmp, "IO_Input A=%0.4X N=%0.2X MRD=%0.2X/%0.2X DATA=%0.2X\n", uP_ADDR, STATE_N210, STATE_MWR_N, prevMWR_N, DATA_OUT);
    Serial.write(tmp);
#endif
  }

  
  //////////////////////////////////////////////////////////////////////
  // Memory Access
  //////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////
  // RD_N Falling Edge
  // Note: We perform actual read operation once during falling
  // edge and then continue to output this value as long as
  // RD_N is low.  This is done to prevent multiple reads from
  // devices like FTDI (not used for here, but I like this
  // method for future use. 
  // Similar method is done for WR_N where we perform the actual
  // write on WR_N rising edge.
  ////////////////////////////////////////////////////////////
  if (STATE_TPB && !STATE_MRD_N && prevMRD_N)     // Falling edge of RD_N
  { 
    // change DATA port to output to uP:
    xDATA_DIR_OUT();

    // Build uP_ADDR from [uP_ADDR_H, uP_ADDR_L]
    uP_ADDR = ADDR();       

    // ROM?
    if ( (ROM_START <= uP_ADDR) && (uP_ADDR <= ROM_END) )
    {
      DATA_latched = rom_bin [ (uP_ADDR - ROM_START) ];
      // Serial.print("ROM ["); Serial.print(uP_ADDR);Serial.print("] = "); Serial.println(DATA_latched);
    }
    else
    // Execute from RAM?
    if ( (RAM_START <= uP_ADDR) && (uP_ADDR <= RAM_END) )
      DATA_latched = RAM[uP_ADDR - RAM_START];
    else
      // Dummy 0xFF (eeprom style) out for unmapped memory locations
      DATA_latched = 0xFF;      

    DATA_OUT = DATA_latched;
    SET_DATA_OUT( DATA_OUT );
    DELAY_FOR_BUFFER();

#if outputDEBUG
    char tmp[50];
    sprintf(tmp, "-- A=%0.4X N=%0.2X D=%0.2X\n", uP_ADDR, STATE_N210, DATA_latched);
    Serial.write(tmp);
#endif

  } 
  else
  ////////////////////////////////////////////////////////////
  // RD_N
  ////////////////////////////////////////////////////////////
  // Continue to output data read on falling edge ^^^
  if (STATE_TPB && !STATE_MRD_N)    
  {    
    // change DATA port to output to uP:
    xDATA_DIR_OUT();

    DATA_OUT = DATA_latched;
    SET_DATA_OUT( DATA_OUT );
    DELAY_FOR_BUFFER();
  } 
  else
  ////////////////////////////////////////////////////////////
  // WR_N Low
  ////////////////////////////////////////////////////////////
  // Start capturing data_in but don't write it to destination yet
  if (!STATE_MWR_N)
  {
    // Teensy read byte from processor
    xDATA_DIR_IN();
    DATA_IN = xDATA_IN();
    
    DATA_latched = DATA_IN;
  } 
  else
  ////////////////////////////////////////////////////////////
  // WR_N Rising Edge
  ////////////////////////////////////////////////////////////
  // Write data to destination when WR# goes high.
  if (STATE_MWR_N && !prevMWR_N)
  {
    // Build uP_ADDR from [uP_ADDR_H, uP_ADDR_L]
    uP_ADDR = ADDR();       

    // Memory Write
    if ( (RAM_START <= uP_ADDR) && (uP_ADDR <= RAM_END) )
      RAM[uP_ADDR - RAM_START] = DATA_latched;

#if outputDEBUG
    char tmp[40];
    sprintf(tmp, "WR A=%0.4X N=%0.2X D=%0.2X\n", uP_ADDR, STATE_N210, DATA_latched);
    Serial.write(tmp);
#endif

  }      

  //////////////////////////////////////////////////////////////////////
  // IO Access for OUTPUT (N2/N1/N0 is 1,2,3,4,5,6,7)
  // which means memory output (databus) will be written to IO device
  // by (TPB * !MRD_N * N) pulse.
  //////////////////////////////////////////////////////////////////////

  if (STATE_N210 && !STATE_MRD_N && STATE_TPB)
  { 
    // Teensy read byte from processor
    xDATA_DIR_IN();
    DATA_IN = xDATA_IN();
    DATA_latched = DATA_IN;

    // Do whatever you want to do DATA_latched based on N210
    
#if outputDEBUG
    char tmp[50];
    sprintf(tmp, "IO_Write A=%0.4X N=%0.2X MWR=%0.2X/%0.2X DATA=%0.2X\n", uP_ADDR, STATE_N210, STATE_MWR_N, prevMRD_N, DATA_latched);
    Serial.write(tmp);
#endif
  }

  //////////////////////////////////////////////////////////////////////
  // start next cycle
  //////////////////////////////////////////////////////////////////////

  // Capture previous states for edge detection
 
  prevMWR_N = STATE_MWR_N;
  prevMRD_N = !STATE_TPB || STATE_MRD_N;     // gate w/ TPB because MRD_N goes low when TPA goes high (can't detect edge).

  CLK_LOW;
  DELAY_FACTOR_L();         // delayMicroseconds(1);
        
#if outputDEBUG
  delay(10);
  Serial.write('#');
#endif

  if (STATE_TPA)      // need to capture address bits when ALE is high.
  {
    uP_ADDR_H = ADDR_H();
  }

  // Limit counter to prevent false TPB after wrap-up.
  if (TPB_cntr > 254)
    TPB_cntr = 254;
    
  // Keep memory databus as output in two scenarios.
  // - 1802 reading from memory
  // - 1802 is writing from memory to IO device, 
  if (STATE_MRD_N && !STATE_N210)
  {
    // DATA_DIR = DIR_IN;
    // Teensy read byte from processor
    xDATA_DIR_IN();
  }
  
}


////////////////////////////////////////
// Soft-UART for 1802's Hard-UART
////////////////////////////////////////

#define SOFT_UART_SAMPLE_COUNTER (30)     
// Instead of calling soft-UART every cpu clock cycle,
// call it every SOFT_UART_SAMPLE_COUNTER cycles.
// reduces overhead...

// This define sets the cpu cycles/baud.
// MCSMP   autobauds => 300
// IDIOT/4 2400baud => 1666

#if (FW_MCSMP20_BASIC3_TINYBASIC)
#define k1802_UART_BAUD (300/SOFT_UART_SAMPLE_COUNTER)
#else
#define k1802_UART_BAUD (1650/SOFT_UART_SAMPLE_COUNTER)
#endif

byte txd_1802;
word txd_delay = k1802_UART_BAUD*1.5;     // start capturing 1.5 bits later, middle
byte txd_bit = 0;
byte prevQ = 1;

byte rxd_1802;
word rxd_delay = k1802_UART_BAUD;         // start output 1 bit at a time
byte rxd_bit = 0;

word char_delay = k1802_UART_BAUD * 40;   // give 1802 time to process uart.
                                          // see below for another assignment.

inline __attribute__((always_inline))
void serialEvent1802()
{
  
  // RXD
  char_delay--;
  
  if (char_delay == 0)
  {
    // Serial.print("@");
    if ((rxd_bit == 0) && (Serial.available()) )
    {
      rxd_bit = 9;        // 7bit + 1 stop
      rxd_1802 = toupper( Serial.read() );
      rxd_delay = k1802_UART_BAUD;
  
      digitalWrite(uP_EF3_N, LOW);      // Start bit, low
      // Serial.print("@"); Serial.print(rxd_1802, HEX); Serial.println(rxd_bit);
    } 
    // Serial.println("-");    
    char_delay = k1802_UART_BAUD * 40;          // <- Update char_delay
  }
  else
  if (rxd_bit)
  {
    rxd_delay--;
    if (rxd_delay == 0)
    {
      digitalWrite(uP_EF3_N, rxd_1802 & 0x01);
      rxd_1802 = (rxd_1802 >> 1);
      rxd_delay = k1802_UART_BAUD;

      // are we done yet?  1bit left, which is stop bit
      rxd_bit--;
      if (rxd_bit == 0x01)
      {
        // set bit0 to output stop bit
        rxd_1802 = 0x01;
      }
      else
      if (rxd_bit == 0) {
        char_delay = k1802_UART_BAUD * 40;          // <- Update char_delay
      }
    }
  }

  // TXD
  // Check for start bit
  if ((txd_bit == 0) && !STATE_Q && prevQ)
  {
    txd_bit  = 9;   // need to receive 7(data)+1(stop) bits
    txd_1802 = 0;   // OR incoming bits to this.
    txd_delay = k1802_UART_BAUD*1.5;
    // Serial.println("Serial.start");
  }
  else
  if (txd_bit)
  {
    txd_delay--;
    if (txd_delay == 0)
    {
      txd_1802 = (txd_1802 >> 1) | (STATE_Q << 7);
      
      txd_delay = k1802_UART_BAUD;

      // are we done yet?  1bit left, which is stop bit
      if ((--txd_bit) == 0x01)
      {
        txd_1802 = (txd_1802 & 0x7F);     // Shift one more to fit 7bits into 8bit.
        Serial.write(txd_1802);
        // no more bits to receive.
        // stop bit will be ignored.
      }
    }
  } 
  
  prevQ = STATE_Q;
}

////////////////////////////////////////////////////////////////////
// Setup
////////////////////////////////////////////////////////////////////

void setup() 
{
  Serial.begin(0);
  while (!Serial);
  
  Serial.write(27);       // ESC command
  Serial.print("[2J");    // clear screen command
  Serial.write(27);
  Serial.print("[H");
  Serial.println("\n");
  Serial.println("Configuration:");
  Serial.println("==============");
  print_teensy_version();  
  Serial.print("Debug:      "); Serial.println(outputDEBUG, HEX);
  Serial.print("SRAM Size:  "); Serial.print(RAM_END - RAM_START + 1, DEC); Serial.println(" Bytes");
  Serial.print("SRAM_START: 0x"); Serial.println(RAM_START, HEX); 
  Serial.print("SRAM_END:   0x"); Serial.println(RAM_END, HEX); 
  Serial.println("");
  Serial.println("=======================================================");
  Serial.println("> RCA Basic Level 3 v1.1 by Ron Cenker, Copyrighted 1981");
  Serial.println("> Tiny Basic by Tom Pittman Copyright 1982");
  Serial.println("> Membership Card Serial Monitor Program     \n\r  by Charles J, Yakym Copyright 2015");
  Serial.println("> The 1802 Membership Card Kit by Lee Hart,  \n\r  Copyrighted 2006");
  Serial.println("> The Membership Card Kit can be purchased at\n\r  http://www.sunrise-ev.com/membershipcard.htm");
  Serial.println("=======================================================");
  Serial.println("");  
  Serial.print  ("==> Soft-UART {Q, EF3#}, Baud Rate: "); 
    Serial.print  (k1802_UART_BAUD, DEC);
    Serial.println(" cpu cycles/bit");
  Serial.println("==> Hit ENTER to start monitor code.");

  // Initialize processor GPIO's
  uP_init();

  // Inject LBR 0x8000 at 0x0000 because this ROM is located at 0x8000 and 1802 starts at 0x0000.
  RAM[0] = 0xC0;
  RAM[1] = 0x80;
  RAM[2] = 0x00;
  
  // Reset processor
  //
  uP_assert_reset();
  for(int i=0;i<50;i++) cpu_tick();
  
  // Go, go, go
  uP_release_reset();

  Serial.println("\n");
}

////////////////////////////////////////////////////////////////////
// Loop()
////////////////////////////////////////////////////////////////////

void loop()
{
  byte i = SOFT_UART_SAMPLE_COUNTER;

  // Loop forever
  //
  while(1)
  {    
    cpu_tick();

    i--;
    if (i == 0)
    {
      serialEvent1802();
      Serial.flush();
      i = SOFT_UART_SAMPLE_COUNTER;
    }
    
#if outputDEBUG
    delay(10);
#endif
  }

}
