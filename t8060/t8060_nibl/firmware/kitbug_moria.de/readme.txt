NIBL-19761127.asm: Source published in 
  Alexander, Mark, "NIBL --Tiny Basic for NAtional's SC/MP Kit - complete
  documentation and annotated source code", Dr. Dobb's Journal of Computer
  Calisthenics and Orthodontia, Vol. 1 Nov./Dec. 1976, pp. 34-50
  with minimal changes for assembling with asl. Communication
  runs at 110 baud with a SC/MP I @ 1 MHz or a SC/MP II @ 2 MHz.
  Characters are echoed with the MSB set.

NIBL-19761217.asm: Source for published NIBL ROM with minimal changes
  for assembling with asl. Communication runs at 110 baud with a SC/MP II
  @ 4 MHz. Characters are echoed with the MSB set.

1200-baud.diff: Patch for NIBL-19761217.asm that changes the baud rate
  to 1200 baud (4 MHz SC/MP II) and echoes characters with the MSB
  cleared.

NIBL-19761217-1200.asm: NIBL-19761217.asm with applied patch

NIBLE.asm: NIBL-19761217.asm patched by Elektor for running it at
  page 1 instead of page. Communication runs at 110 baud with a SC/MP II
  @ 2 MHz. Characters are echoed with the MSB set. Since the publication
  in Elektor mentioned about 300 modifications, probably the ROM was
  patched and this source code never existed.

orig/NIBL.bin: NIBL ROM from National Semiconductor
orig/NIBLE.bin: NIBLE ROM from Elektor

NIBLE.bin speed changes:

address  110 300 600 1200 @ 2 MHz SC/MP II
1f85      57  76  a7   3d
1f87      04  01  00   00
1f94      7e  e5  45   76
1f96      08  02  01   00
1fb9      08  06  04   02
1fc4      ff  64  25   86
1fc6      17  06  03   01
1fd0      8a  f0  50   81
1fd2      08  02  01   00

Changes after ROM publication:

Elektor:
"In NIBL-E Table 1 in line 1280 the second bytes should be 0E not 1E.
Means ERROR AT is displayed instead of BRK AT (at end of prog or on BREAK key)"

Roger Marin: In CMPR the statement OR HI(P2) was later changed to ANI 01.

Roger Marin: In UNTIL  the statement OR HI(P2) was later changed to ANI 01.

