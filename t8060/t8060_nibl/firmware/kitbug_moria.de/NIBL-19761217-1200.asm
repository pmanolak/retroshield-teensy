	.TITLE  NIBL,'12/17/76'
	.LIST	1

;******************************************************
;*     WE ARE TIED DOWN TO A LANGUAGE WHICH           *
;*     MAKES UP IN OBSCURITY WHAT IT LACKS            *
;*     IN STYLE.                                      *
;*                     - TOM STOPPARD                 *
;*                                                    *
;******************************************************
 
TSTBIT	=	020		;I.L. INSTRUCTION FLAGS
JMPBIT	=	040
CALBIT	=	080
P1	=	1               ;SC/MP POINTER ASSIGNMENTS
P2	=	2
P3	=	3
EREG	=	-128            ;THE EXTENSION REGISTER
 
; DISPLACEMENTS FOR RAM VARIABLES USED BY INTERPRETER
 
DOPTR	=	-1              ;DO-STACK POINTER
FORPTR	=	-2              ;FOR-STACK POINTER
LSTK	=	-3              ;ARITHMETIC STACK POINTER
SBRPTR	=	-4              ;GOSUB STACK POINTER
PCLOW	=	-5              ;I.L. PROGRAM COUNTER
PCHIGH	=	-6
PCSTK	=	-7              ;I.L. CALL STACK POINTER
LOLINE	=	-8              ;CURRENT LINE NUMBER
HILINE	=	-9
PAGE	=	-10             ;VALUE OF CURRENT PAGE
LISTNG	=	-11             ;LISTING FLAG
RUNMOD	=	-12             ;RUN/EDIT FLAG
LABLLO	=	-13
LABLHI	=	-14
P1LOW	=	-15             ;SPACE TO SAVE CURSOR
P1HIGH	=	-16
LO	=	-17
HI	=	-18
FAILLO	=	-19
FAILHI	=	-20
NUM	=	-21
TEMP	=	-22
TEMP2	=	-23
TEMP3	=	-24
CHRNUM	=	-25
RNDF	=	-26
RNDX	=	-27             ;SEEDS FOR RANDOM NUMBER
RNDY	=	-28
 
; ALLOCATION OF RAM FOR NIBL VARIABLES, STACKS,
;  AND LINE BUFFER
        
	.=01000+28
VARS:	.=.+52			; NIBL VARIABLES A-Z
AESTK:	.=.+26             	; ARITHMETIC STACK  
SBRSTK:	.=.+16			;GOSUB STACK
DOSTAK:	.=.+16			;DO/UNTIL STACK   
FORSTK:	.=.+28			;FOR/NEXT STACK    
PCSTAK:	.=.+48			;I.L. CALL STACK    
LBUF:	.=.+74			;LINE BUFFER     
PGM:	.=0			;USER'S PROGRAM            
      
	.MACRO	LDPI,P,VAL
	.MLOC	TEMP
	LDI	H(VAL)
	XPAH	P
	LDI	L(VAL)
	XPAL	P
	.ENDM

 
;*************************************
;*      INITIALIZATION OF NIBL       *
;*************************************
          
 
	NOP
	LDPI	P2,VARS		;POINT P2 AT VARIABLES
	LDPI    P1,PGM		;POINT P1 AT PAGE ONE PROGRAM
	LDI     -1              ;STORE -1 AT START OF PROGRAM
	ST      0(P1)
	ST      1(P1)
	LDI     0D		;ALSO STORE A DUMMY
	ST      -1(P1)          ;  CARRIAGE RETURN
	LDI     2		;POINT P2 AT PAGE 2,
	ST      PAGE(P2)	; INITIALLY SET PAGE TO 2
	XPAL    P1
	LDI     020
	XPAH    P1
	DLD     2(P1)		;CHECK IF THERE IS REALLY
	XAE			; A PROGRAM IN PAGE 2:
	LD      EREG(P1)	; IF FIRST LINE LENGTH
	XRI     0D		; POINTS TO CARR. RETURN
	JZ	$0		; AT END OF LINE
	DLD     PAGE(P2)	;IF NOT, PAGE = 1
$0:	LDI     020
$LOOP:	XPAH    P1
	LDI     -1		;STORE -1 IN 2 CONSECUTIVE
        ST      (P1)		; LOCATIONS AT START OF PAGE
        ST      1(P1)
        LDI     0D		;ALSO PUT A DUMMY END-OF-LINE
        ST      -1(P1)		; JUST BEFORE TEXT
        XPAH    P1		;UPDATE P1 TO POINT TO
        CCL			; NEXT PAGE (UNTIL PAGE=8)
        ADI     010             ;REPEAT INITIALIZATION
        XRI     080             ; FOR PAGES 2-7
        JZ      $1
        XRI     080
        JMP     $LOOP
$1:	LDI     0		;CLEAR SOME FLAGS
        ST      RUNMOD(P2)
        ST      LISTNG(P2)
        LDI     L(BEGIN)	;INITIALIZE IL PC SO THAT
        ST      PCLOW(P2)	; NIBL PROGRAM
        LDI     H(BEGIN)	; IS EXECUTED IMMEDIATELY
        ST      PCHIGH(P2)
CLEAR:  LDI     0
        ST      TEMP(P2)
        XAE
CLEAR1: LDI     0		;SET ALL VARIABLES
        ST      EREG(P2)	; TO ZERO
        ILD     TEMP(P2)
        XAE
        LDI     52
        XRE
        JNZ     CLEAR1
        LDI     L(AESTK)	;INITIALIZE SOME STACKS:
        ST      LSTK(P2)        ; ARITHMETIC STACK,
        LDI     L(DOSTAK)
        ST      DOPTR(P2)       ; DO/UNTIL STACK,
        LDI     L(SBRSTK)
        ST      SBRPTR(P2)      ; GOSUB STACK,
        LDI     L(PCSTAK)
        ST      PCSTK(P2)       ; I.L. CALL STACK,
        LDI     L(FORSTK)
        ST      FORPTR(P2)      ; FOR/NEXT STACK
 
 
;*************************************
;*   INTERMEDIATE LANGUAGE EXECUTOR  *
;*************************************
 
EXECIL: LD      PCLOW(P2)       ;SET P3 TO CURRENT
        XPAL    P3              ; IL PC.
        LD      PCHIGH(P2)
        XPAH    P3
CHEAT:  LD      @1(P3)
        XAE                     ;GET NEW I.L. INSTRUCTION
        LD      @1(P3)          ; INTO P3 THROUGH
        XPAL    P3              ; OBSCURE METHODS
        ST      PCLOW(P2)       ;SIMULTANEOUSLY, INCREMENT
        LDE                     ; THE I.L. PC BY 2
        ANI     0F              ;REMOVE FLAG FROM INSTRUCTION
        ORI     ./256           ; TURN INTO ACTUAL ADDRESS,
        XPAH    P3              ; PUT BACK INTO P3
        ST      PCHIGH(P2)
        LDE
        ANI     0F0             ;CHECK IF I.L. INSTRUCTION
        XRI     TSTBIT          ; IS A 'TEST'
        JZ      TST
        XRI     CALBIT!TSTBIT   ;CHECK FOR I.L. CALL
        JZ      ILCALL
        XRI     JMPBIT!CALBIT   ;CHECK FOR I.L. JUMP
	JZ      CHEAT           ;I.L. JUMP IS TRIVIAL
NOJUMP: XPPC    P3              ;MUST BE AN ML SUBROUTINE
        JMP     EXECIL          ; IF NONE OF THE ABOVE
 
 
;*************************************
;*     INTERMEDIATE LANGUAGE CALL    *
;*************************************
 
ILCALL: LD      PCSTK(P2)
        XRI     L(LBUF)         ;CHECK FOR STACK OVERFLOW
        JNZ     ILC1
        LDI     10         
        JMP     E0A
ILC1:   XRI     L(LBUF)         ;RESTORE ACCUMULATOR
        XPAL    P3              ;SAVE LOW BYTE OF NEW
        ST      TEMP(P2)        ; I.L. PC IN TEMP
        LDI     H(PCSTAK)       ;POINT P3 AT I.L.
        XPAH    P3              ; SUBROUTINE STACK
        XAE                     ;SAVE NEW I.L.  PC HIGH IN EX
        LD      PCLOW(P2)       ;SAVE OLD I.L.  PC ON STACK
        ST      @1(P3)
        LD      PCHIGH(P2)
        ST      @1(P3)
        LD      TEMP(P2)        ;GET LOW BYTE OF NEW
        XPAL    P3              ; I.L. PC INTO P3 LOW
        ST      PCSTK(P2)       ;UPDATE I.L. STACK POINTER
        LDE                     ;GET HIGH BYTE OF NEW P3
        XPAH    P3              ; I.L. PC INTO P3 HIGH
CHEAT1:	JMP     CHEAT
 
 
;*************************************
;*      I.L. 'TEST' INSTRUCTION      *
;*************************************
 
	.LOCAL
TST:    ST      CHRNUM(P2)      ;CLEAR NUMBER OF CHARS SCANNED
$SCAN:  LD      @1(P1)          ;SLEW OFF SPACES
        XRI     ' '
        JZ      $SCAN
        LD      @-1(P1)         ;REPOSITION CURSOR
        LD      PCHIGH(P2)      ;POINT P3 AT IL TABLE
        XPAH    P3
        ST      FAILHI(P2)      ;FAIL ADDRESS <- OLD P3
        LD      PCLOW(P2)
        XPAL    P3
        ST      FAILLO(P2)
$LOOP:  LD      @1(P3)
        XAE                     ;SAVE CHAR FROM TABLE
        DLD     CHRNUM(P2)      ;DECREMENT CHAR COUNT
        LDE                     ;GET CHAR BACK
        ANI     07F             ;SCRUB OFF FLAG (IF ANY)
        XOR     @1(P1)          ;IS CHAR EQUAL TO TEXT CHAR?
        JNZ     $NEQ            ;NO - END TEST
        LDE                     ;YES - BUT IS IT LAST CHAR?
        JP      $LOOP           ;IF NOT, CONTINUE TO COMPARE
        JMP     CHEAT           ;IF SO, GET NEXT I. L.  
X0:     JMP     EXECIL          ; INSTRUCTION
$NEQ:   LD      CHRNUM(P2)      ;RESTORE P1 TO
        XAE                     ; ORIGINAL VALUE         
        LD      @EREG(P1)
        LD      FAILLO(P2)      ;LOAD TEST-FAIL ADDRESS
        XPAL    P3              ; INTO P3
        LD      FAILHI(P2)
        XPAH    P3
        JMP     CHEAT1          ;GET NEXT IL INSTRUCTION
 
 
;*************************************
;*       I.L. SUBROUTINE RETURN      *
;*************************************
 
RTN:    LDI     H(PCSTAK)	;POINT P3 AT I.L. PC STACK
        XPAH    P3
        LD      PCSTK(P2)
        XPAL    P3
        LD      @-1(P3)         ;GET HIGH PART OF OLD PC
        XAE
        LD      @-1(P3)         ;GET LOW PART OF OLD PC
        XPAL    P3
        ST      PCSTK(P2)       ;UPDATE IL STACK POINTER
        LDE
        XPAH    P3              ;P3 NOW HAS OLD IL PC
        JMP     CHEAT1
E0A:    JMP     E0

 
;*************************************
;*     SAVE GOSUB RETURN ADDRESS     *
;*************************************
 
SAV:    LD      SBRPTR(P2)
        XRI     L(DOSTAK)	;CHECK FOR MORE
        JZ      SAV2            ; THAN 8 SAVES
        ILD     SBRPTR(P2)
        ILD     SBRPTR(P2)
        XPAL    P3              ;SET P3 TO
        LDI     H(SBRSTK)       ; SUBROUTINE STACK TOP.
        XPAH    P3
        LD      RUNMOD(P2)      ;IF IMMEDIATE MODE,
        JZ      SAV1            ; SAVE NEGATIVE ADDRESS.
        XPAH    P1              ;SAVE HIGH PORTION
        ST      -1(P3)          ; OF CURSOR
        XPAH    P1
        XPAL    P1              ;SAVE LOW PORTION
        ST      -2(P3)          ; OF CURSOR
        XPAL    P1
        JMP     X0              ;RETURN
SAV1:   LDI     -1              ;IMMEDIATE MODE
        ST      -1(P3)          ; RETURN ADDRESS IS
        JMP     X0              ; NEGATIVE.
SAV2:   LDI     10              ;ERROR: MORE THAN
        JMP     E0              ; 8 GOSUBS

 
;*************************************
;*     CHECK STATEMENT FINISHED      *                      
;*************************************
 
DONE:   LD      @1(P1)          ;SKIP SPACES  
        XRI     ' '
        JZ      DONE
        XRI     ' ' ! 0D        ;IS IT CARRIAGE RETURN?
        JZ      DONE1           ;YES - RETURN
        XRI     037             ;IS CHAR A ':' ?
        JNZ     DONE2           ;NO - ERROR
DONE1:  XPPC    P3              ;YES - RETURN
DONE2:  LDI     4
        JMP     E0
 
 
;*************************************
;*     RETURN FROM GOSUB             *
;*************************************
 
RSTR:   LD      SBRPTR(P2)
        XRI     L(SBRSTK)       ;CHECK FOR RETURN
        JNZ     RSTR1           ; W/O GOSUB
        LDI     9
E0:     JMP     E1              ;GOTO ERROR.
RSTR1:  DLD     SBRPTR(P2)
        DLD     SBRPTR(P2)      ;POP GOSUB STACK,
        XPAL    P3              ; PUT PTR INTO P3
        LDI     H(SBRSTK)
        XPAH    P3
        LD      1(P3)           ;IF ADDRESS NEGATIVE,
        JP      RSTR2           ; SUBROUTINE WAS CALLED
        LDI     0               ; IN IMMEDIATE MODE,
        ST      RUNMOD(P2)      ; SO FINISH UP EXECUTING
X1:     JMP     X0
RSTR2:  XPAH    P1              ;RESTORE CURSOR HIGH
        LD      0(P3)
        XPAL    P1              ;RESTORE CURSOR LOW
        LDI     1               ;SET RUN MODE
        ST      RUNMOD(P2)
        JMP     X1
 
 
;*************************************
;*     TRANSFER TO NEW STATEMENT     *
;*************************************
 
XFER:   LD       LABLHI(P2)     ;CHECK FOR NON-EXISTENT LINE
        JP       XFER1
        LDI      8
        JMP      E1
XFER1:  LDI      1              ;SET RUN MODE TO 1
        ST       RUNMOD(P2)
        XPPC     P3
 
 
;*************************************
;*     PRINT STRING IN TEXT          *
;*************************************
 
PRS:    LDPI     P3,PUTC-1      ;POINT P3 AT PUTC ROUTINE
        LD       @1(P1)         ;L0AD NEXT CHAR
        XRI      '"'            ;IF ", END OF
        JZ       X1             ; STRING
        XRI      02F            ;IF CR, ERROR
        JZ       PRS1
        XRI      0D             ;RESTORE CHAR
        XPPC     P3             ;PRINT CHAR
        JMP      PRS            ;GET NEXT CHAR
PRS1:   LDI      7              ;SYNTAX ERROR
E1:     JMP      E2
 
 
;*************************************
;*     PRINT NUMBER ON STACK         *
;*************************************
 
; THIS ROUTINE IS BASED ON DENNIS ALLISON'S BINARY TO DECIMAL
; CONVERSION ROUTINE IN VOL. 1, #1 OF "DR. DOBB'S JOURNAL",
; BUT IS MUCH MORE OBSCURE BECAUSE OF THE STACK MANIPULATION.

	.LOCAL 
PRN:    LDI     H(AESTK)	;POINT P3 AT A.E. STACK
        XPAH    P3
        ILD     LSTK(P2)
        ILD     LSTK(P2)
        XPAL    P3
        LDI     10              ;PUT 10 ON STACK (WE'LL BE
        ST      -2(P3)          ; DIVIDING BY IT LATER)
        LDI     0     
        ST      -1(P3)
        LDI     5               ;SET CHRNUM TO POINT TO PLACE
        ST      CHRNUM(P2)      ; IN STACK WHERE WE STORE
        LDI     -1              ; THE CHARACTERS TO PRINT
        ST      5(P3)           ;FIRST CHAR IS A FLAG (-1)
        LD      -3(P3)          ;CHECK IF NUMBER IS NEGATIVE
        JP      $1
        LDI     '-'             ;PUT '-' ON STACK, AND NEGATE
        ST      4(P3)           ; THE NUMBER
        LDI     0
        SCL
        CAD     -4(P3)
        ST      -4(P3)
        LDI     0
        CAD     -3(P3)
        ST      -3(P3)
        JMP     X1              ;GO DO DIVISION BY  10
$1:     LDI     ' '             ;IF POSITIVE, PUT ' ' ON
        ST      4(P3)           ; STACK BEFORE DIVISION
X4:     JMP     X1
E2:     JMP     ERR1
                                                               
; THE DIVISION IS PERFORMED, THEN CONTROL IS TRANSFERRED
; TO PRN1, WHICH FOLLOWS.
 
PRN1:   ILD     LSTK(P2)        ;POINT P1 AT A.E. STACK
        ILD     LSTK(P2)
        XPAL    P1
        LDI     H(AESTK)
        XPAH    P1
        ILD     CHRNUM(P2)      ;INCREMENT CHARACTER STACK
        XAE                     ; POINTER, PUT IN EX. REG.
        LD      1(P1)           ;GET REMAINDER FROM DIVIDE,
        ORI     '0'
        ST      EREG(P1)        ; PUT IT ON THE STACK
        LD      -3(P1)          ;IS THE QUOTIENT ZERO YET?
        OR      -4(P1)
        JZ      $PRNT           ;YES - GO PRINT THE NUMBER
        LDI     H(PRNUM1)       ;NO - CHANGE THE I.L. PC
        ST      PCHIGH(P2)      ; SO THAT DIVIDE IS
        LDI     L(PRNUM1)       ; PERFORMED AGAIN
        ST      PCLOW(P2)
        JMP     X4              ;GO DO DIVISION BY 10 AGAIN
$PRNT:  LDPI    P3,PUTC-1       ;POINT P3 AT PUTC ROUTINE
        LD      LISTNG(P2)      ;IF LISTING, SKIP PRINTING
        JNZ     $2              ; LEADING SPACE
        LD      4(P1)           ;PRINT EITHER '-'
        XPPC    P3              ; OR LEADING SPACE
        LD      CHRNUM(P2)      ;GET EX. REG. VALUE BACK
        XAE
$2:     LD      @EREG(P1)       ;POINT P3 AT FIRST CHAR
        LD      (P1)            ; TO BE PRINTED
$LOOP:  XPPC    P3              ;PRINT THE CHARACTER
        LD      @-1(P1)         ;GET NEXT CHARACTER
        JP      $LOOP           ;REPEAT UNTIL = -1
        LDI     L(AESTK)
        ST      LSTK(P2)        ;CLEAR THE A.E. STACK
        LD      LISTNG(P2)      ;PRINT A TRAILING SPACE
        JNZ     X4              ; IF NOT LISTING PROGRAM
        LDI     ' '
        XPPC    P3
        JMP     X4
 
 
;*************************************
;*     CARRIAGE RETURN/LINE FEED     *
;*************************************
 
NLINE:  LDPI    P3,PUTC-1       ;POINT P3 AT PUTC ROUTINE
        LDI     0D              ;CARRIAGE RETURN
        XPPC    P3
        LDI     0A              ;LINE FEED
        XPPC    P3
X5:     JMP     X4
                                                              
 
;*************************************
;*      ERROR  ROUTINE               *
;*************************************

	.LOCAL 
ERR:    LDI     5               ;SYNTAX ERROR
ERR1:   ST      NUM(P2)         ;SAVE ERROR #
ERR2:   LD      NUM(P2)
        ST      TEMP(P2)
        LDPI    P3,PUTC-1       ;POINT P3 AT PUTC
        LDI     0D              ;PRINT CR/LF
        XPPC    P3
        LDI     0A
        XPPC    P3
        LDPI    P1,MESGS        ;P1 -> ERROR MESSAGES
$1:     DLD     NUM(P2)         ;IS THIS THE RIGHT MESSAGE?
        JZ      $MSG            ;YES - GO PRINT IT
$LOOP:  LD      @1(P1)          ;NO - SCAN THROUGH TO
        JP      $LOOP           ; NEXT MESSAGE
        JMP     $1
$MSG:   LD      @1(P1)          ;GET MESSAGE CHAR
        XPPC    P3              ;PRINT IT
        LD      -1(P1)          ;IS MESSAGE DONE?
        JP      $MSG            ;NO - GET NEXT CHAR
        LD      TEMP(P2)        ;WAS THIS A BREAK MESSAGE?
        XRI     14
        JZ      $3              ;YES - SKIP PRINTING 'ERROR'
        LDPI    P1,MESGS        ;NO - PRINT ERROR
$2:     LD      @1(P1)          ;GET CHARACTER
        XPPC    P3              ;PRINT IT
        LD      -1(P1)          ;DONE?
        JP      $2              ;NO - REPEAT LOOP
$3:     LD      RUNMOD(P2)      ;DON'T PRINT LINE #
        JZ      FIN             ; IF IMMEDIATE MODE
        LDI     ' '
        XPPC    P3              ;SPACE
        LDI     'A'             ;AT
        XPPC    P3
        LDI     'T'
        XPPC    P3
        LDI     H(AESTK)        ;POINT P3 AT A.E. STACK
        XPAH    P3
        ILD     LSTK(P2)
        ILD     LSTK(P2)
        XPAL    P3
        LD      HILINE(P2)      ;GET HIGH BYTE OF LINE #
        ST      -1(P3)          ;PUT ON STACK
        LD      LOLINE(P2)      ;GET LOW BYTE OF LINE #
        ST      -2(P3)          ;PUT ON STACK
        LDI     L(ERRNUM)       ;GO TO PRN
        ST      PCLOW(P2)
        LDI     H(ERRNUM)                                  
        ST      PCHIGH(P2)
X5A:    JMP     X5
 
 
;*************************************
;*      BREAK, NXT, FIN, & STRT      *
;*************************************
 
BREAK:  LDI     14              ;*** CAUSE A BREAK ***
E3A:    JMP     ERR1
                                ;*** NEXT STATEMENT ***
NXT:    LD      RUNMOD(P2)      ;IF IN IMMED. MODE,
        JZ      FIN             ; STOP EXECUTION
        LD      (P1)            ;IF WE HIT END OF FILE,
        ANI     080             ; FINISH UP THINGS
        JNZ     FIN
        CSA                     ;BREAK IF SOMEONE IS
        ANI     020             ; TYPING ON THE CONSOLE
        JZ      BREAK
        LD      -1(P1)          ;GET LAST CHARACTER SCANNED
        XRI     0D              ;WAS IT CARRIAGE RETURN?
        JNZ     NXT1            ;YES - SKIP FOLLOWING UPDATES
        LD      @1(P1)          ;GET HIGH BYTE OF NEXT LINE #
        ST      HILINE(P2)      ;SAVE IT
        LD      @2(P1)          ;GET LOW BYTE OF LINE #, SKIP
        ST      LOLINE(P2)      ; LINE LENGTH BYTE
NXT1:   LDI     H(STMT)         ;GO TO 'STMT' IN IL TABLE
        ST      PCHIGH(P2)
        LDI     L(STMT)
        ST      PCLOW(P2)
        XPPC    P3
 
FIN:    LDI     0               ;*** FINISH EXECUTION ***
        ST      RUNMOD(P2)      ;CLEAR RUN MODE
        LDI     L(AESTK)        ;CLEAR ARITHMETIC STACK
        ST      LSTK(P2)
        LDI     L(START)        ;SET IL PC TO GETTING LINES
        ST      PCLOW(P2)       ; TO PROMPT FOR COMMAND
        LDI     H(START)
        ST      PCHIGH(P2)
        LDI     L(PCSTAK)
        ST      PCSTK(P2)
        JMP     X5A
                                ;*** START EXECUTION ***
STRT:   ILD     RUNMOD(P2)      ;RUN MODE = 1
        LD      TEMP2(P2)       ;POINT CURSOR TO
        XPAH    P1              ; START OF NIBL PROGRAM
        LD      TEMP3(P2)
        XPAL    P1
        LDI     L(SBRSTK)       ;EMPTY SOME STACKS:
        ST      SBRPTR(P2)      ; GOSUB STACK,
        LDI     L(FORSTK)
        ST      FORPTR(P2)      ; FOR STACK
        LDI     L(DOSTAK)
        ST      DOPTR(P2)       ; & DO/UNTIL STACK
        XPPC    P3              ;RETURN
X6:     JMP     X5A
E4:     JMP     E3A
 
 
;*************************************
;*        LIST NIBL PROGRAM          *
;*************************************
 
LST:    LD      (P1)            ;CHECK FOR END OF FILE
        XRI     080
        JP      LST2
        LDI     H(AESTK)        ;GET LINE NUMBER ONTO STACK
        XPAH    P3
        ILD     LSTK(P2)
        ILD     LSTK(P2)
        XPAL    P3
        LD      @1(P1)
        ST      -1(P3)   
        LD      @1(P1)
        ST      -2(P3)
        LD      @1(P1)          ;SKIP OVER LINE LENGTH
        LDI     1
        ST      LISTNG(P2)      ;SET LISTING FLAG
        JMP     X6              ;GO PRINT LINE NUMBER
LST2:   LDI     0
        ST      LISTNG(P2)      ;CLEAR LISTING FLAG
        JS      P3,NXT          ;GO TO NXT
X6A:    JMP     X6
E5:     JMP     E4
LST3:   LDPI    P3,PUTC-1       ;POINT P3 AT PUTC
LST4:   CSA
        ANI     020
        JZ      LST2            ;IF TYPING, STOP
        LD      @1(P1)          ;GET NEXT CHAR
        XRI     0D              ;TEST FOR CR
        JZ      LST5
        XRI     0D              ;GET CHARACTER
        XPPC    P3              ;PRINT CHARACTER
        JMP     LST4
LST5:   LDI     0D              ;CARRIAGE RETURN
        XPPC    P3
        LDI     0A              ;LINE FEED
        XPPC    P3
        CCL
        LDI     L(LIST3)
        ST      PCLOW(P2)
        LDI     H(LIST3)
        ST      PCHIGH(P2)
        JMP     LST             ;GET NEXT LINE
 
 
;*************************************
;*          ADD AND SUBTRACT         *
;*************************************
 
ADD:    LDI     H(AESTK)        ;SET P3 TO CURRENT-
        XPAH    P3              ; STACK LOCATION
        DLD     LSTK(P2)
        DLD     LSTK(P2)
        XPAL    P3
        CCL
        LD      -2(P3)          ;REPLACE TWO TOP ITEMS
        ADD     0(P3)           ; ON STACK BY THEIR SUM
        ST      -2(P3)
        LD      -1(P3)
        ADD     1(P3)
        ST      -1(P3)
X7:     JMP     X6A
 
SUB:    LDI     H(AESTK)        ;SET P3 TO CURRENT
        XPAH    P3              ; STACK LOCATION
        DLD     LSTK(P2)
        DLD     LSTK(P2)
        XPAL    P3
        SCL
        LD      -2(P3)          ;REPLACE TWO TOP ITEMS
        CAD     0(P3)           ; ON STACK BY THEIR DIFFERENCE
        ST      -2(P3)
        LD      -1(P3)
        CAD     1(P3)
        ST      -1(P3)
        JMP     X6A
 
 
;*************************************
;*          NEGATE                   *
;*************************************
 
NEG:    LDI     H(AESTK)        ;SET P3 TO CURRENT
        XPAH    P3              ; STACK LOCATION
        LD      LSTK(P2)
        XPAL    P3
        SCL
        LDI     0
        CAD     -2(P3)          ;NEGATE TOP ITEM ON STACK
        ST      -2(P3)
        LDI     0
        CAD     -1(P3)
        ST      -1(P3)
X8:     JMP     X7
E6:     JMP     E5
 
 
;*************************************
;*        MULTIPLY                   *
;*************************************

	.LOCAL 
MUL:    LDI     H(AESTK)        ;SET P3 TO CURRENT
        XPAH    P3              ; STACK LOCATION
        LD      LSTK(P2)
        XPAL    P3              ;DETERMINE SIGN OF PRODUCT, 
        LD      -1(P3)          ; SAVE IN TEMP(P2)
        XOR     -3(P3)
        ST      TEMP(P2)
        LD      -1(P3)          ;CHECK FOR NEGATIVE
        JP      $1              ; MULTIPLIER
        SCL
        LDI     0               ;IF NEGATIVE,
        CAD     -2(P3)          ; NEGATE
        ST      -2(P3)
        LDI     0
        CAD     -1(P3)
        ST      -1(P3)
$1:     LD      -3(P3)          ;CHECK FOR NEGATIVE
        JP      $2              ; MULTIPLICAND
        SCL
        LDI     0               ;IF NEGATIVE,
        CAD     -4(P3)          ; NEGATE
        ST      -4(P3)
        LDI     0
        CAD     -3(P3)
        ST      -3(P3)
$2:     LDI     0               ;CLEAR WORKSPACE
        ST      0(P3)
        ST      1(P3)
        ST      2(P3)
        ST      3(P3)
        LDI     16              ;SET COUNTER TO 16
        ST      NUM(P2)
$LOOP:  LD      -1(P3)          ;ROTATE MULTIPLIER
        RRL                     ; RIGHT ONE BIT
        ST      -1(P3)
        LD      -2(P3)
        RRL
        ST      -2(P3)
        CSA                     ;CHECK FOR CARRY BIT
        JP      $3              ;IF NOT SET, DON'T DO ADD
        CCL
        LD      2(P3)           ;ADD MULTIPLICAND
        ADD     -4(P3)          ; INTO WORKSPACE
        ST      2(P3)
        LD      3(P3)
        ADD     -3(P3)
        ST      3(P3)
        JMP     $3
E6A:    JMP     E6
$3:     CCL
        LD      3(P3)           ;SHIFT WORKSPACE RIGHT BY 1
        RRL
        ST      3(P3)
        LD      2(P3)
        RRL
        ST      2(P3)
        LD      1(P3)
        RRL
        ST      1(P3)
        LD      0(P3)
        RRL
        ST      0(P3)
        DLD     NUM(P2)         ;DECREMENT COUNTER
        JNZ     $LOOP           ;LOOP IF NOT ZERO
        JMP     $4
X9:     JMP     X8
$4:     LD      TEMP(P2)        ;CHECK SIGN WORD
        JP      $EXIT           ;IF BIT7 = 1, NEGATE PRODUCT
        SCL
        LDI     0
        CAD     0(P3)
        ST      0(P3)
        LDI     0
        CAD     1(P3)
        ST      1(P3)
$EXIT:  LD      0(P3)           ;PUT PRODUCT ON TOP
        ST      -4(P3)          ; OF STACK
        LD      1(P3)
        ST      -3(P3)
        DLD     LSTK(P2)        ;SUBTRACT 2 FROM
        DLD     LSTK(P2)        ; LSTK
        JMP     X9

 
;*************************************
;*            DIVIDE                 *
;*************************************

	.LOCAL 
DIV:    LDI     H(AESTK)
        XPAH    P3
        LD      LSTK(P2)  
        XPAL    P3
        LD      -1(P3)          ;CHECK FOR DIVISION BY 0
        OR      -2(P3)
        JNZ     $0
        LDI     13
        JMP     E6A
$0:     LD      -3(P3)
        XOR     -1(P3)
        ST      TEMP(P2)        ;SAVE SIGN OF QUOTIENT
        LD      -3(P3)          ; IS DIVIDEND POSITIVE?
        JP      $POS            ;YES - JUMP
        LDI     0
        SCL
        CAD     -4(P3)          ;NO - NEGATE DIVIDEND,
        ST      3(P3)           ; STORE IN RIGHT HALF
        LDI     0               ; OF 32-BIT ACCUMULATOR
        CAD     -3(P3)
        ST      2(P3)
        JMP     $1
X9A:    JMP     X9
$POS:   LD      -3(P3)          ;STORE NON-NEGATED DIVIDEND
        ST      2(P3)           ; IN 32-BIT ACCUMULATOR
        LD      -4(P3)
        ST      3(P3)
$1:     LD      -1(P3)          ;CHECK FOR NEGATIVE DIVISOR
        JP      $2
        LDI     0               ;NEGATE DIVISOR
        SCL
        CAD     -2(P3)
        ST      -2(P3)
        LDI     0
        CAD     -1(P3)
        ST      -1(P3)
$2:     LDI     0               ;PUT ZERO IN:
        ST      1(P3)           ;  LEFT HALF OF 32-BIT ACC,
        ST      0(P3)
        ST      NUM(P2)         ;  THE COUNTER, AND
        ST      -3(P3)          ;  IN THE DIVIDEND, NOW USED
        ST      -4(P3)          ;  STORE THE QUOTIENT
$LOOP:  CCL                     ;BEGIN MAIN DIVIDE LOOP:
        LD      -4(P3)          ;  SHIFT QUOTIENT LEFT,
        ADD     -4(P3)
        ST      -4(P3)                       
        LD      -3(P3)
        ADD     -3(P3)
        ST      -3(P3)
        CCL                     ;  SHIFT 32-BIT ACC LEFT,
        LD      3(P3)
        ADD     3(P3)
        ST      3(P3)                                    
        LD      2(P3)
        ADD     2(P3)
        ST      2(P3)
        LD      1(P3)
        ADD     1(P3)
        ST      1(P3)
        LD      (P3)
        ADD     (P3)
        ST      (P3)
        SCL
        LD      1(P3)           ;  SUBTRACT DIVISOR INTO
        CAD     -2(P3)          ;   LEFT HALF OF ACC,   
        ST      1(P3)
        LD      (P3)
        CAD     -1(P3)
        ST      (P3)
        JP      $ENT1           ;  IF RESULT IS NEGATIVE,
        CCL                     ;   RESTORE ORIGINAL CONTENTS
        LD      1(P3)           ;   OF ACC BY ADDING DIVISOR
        ADD     -2(P3)
        ST      1(P3)
        LD      (P3)
        ADD     -1(P3)
        ST      (P3)
        JMP     $3
X9B:    JMP     X9A
$ENT1:  LD      -4(P3)          ;  ELSE IF RESULT POSITIVE,
        ORI     1               ;   RECORD A 1 IN QUOTIENT
        ST      -4(P3)          ;   W/O RESTORING THE ACC
$3:     ILD     NUM(P2)         ;  INCREMENT THE COUNTER
        XRI     16              ;  ARE WE DONE?
        JNZ     $LOOP           ;  LOOP IF NOT DONE
        LD      TEMP(P2)        ;CHECK THE QUOTIENT'S SIGN,
        JP      $END            ; NEGATING IF NECESSARY
        LDI     0
        SCL
        CAD     -4(P3) 
        ST      -4(P3)
        LDI     0
        CAD     -3(P3)
        ST      -3(P3)
$END:   DLD     LSTK(P2)        ;DECREMENT THE STACK POINTER,
        DLD     LSTK(P2)
        JMP     X9B             ; AND EXIT
 
 
;*************************************
;*         STORE VARIABLE            *
;*************************************
 
STORE:  LDI     H(AESTK)        ;SET P3 TO STACK
        XPAH    P3
        LD      LSTK(P2)
        XPAL    P3
        LD      @-3(P3)         ;GET VARIABLE INDEX
        XAE                     ;PUT IN E REG
        LD      1(P3)
        ST      EREG(P2)        ;STORE LOWER 8 BITS
        CCL                     ; INTO VARIABLE
        LDE                     ;INCREMENT INDEX
        ADI     1
        XAE
        LD      2(P3)
        ST      EREG(P2)        ;STORE UPPER 8 BITS
        XPAL    P3              ; INTO VARIABLE
        ST      LSTK(P2)        ;UPDATE STACK POINTER
X10:    JS      P3,EXECIL
 
 
;*************************************
;*     TEST FOR VARIABLE IN TEXT     *
;*************************************
 
TSTVAR: LD      @1(P1)
        XRI     ' '             ;SLEW OFF SPACES
        JZ      TSTVAR 
        LD      -1(P1)          ;CHARACTER IN QUESTION
        SCL
        CAI     'Z'+1           ;SUBTRACT 'Z'+l
        JP      $FAIL           ;NOT VARIABLE IF POSITIVE
        SCL
        CAI     'A'-'Z'-1       ;SUBTRACT 'A'
        JP      $MAYBE          ;IF POS, MAY BE VARIABLE
$FAIL:  LD      @-1(P1)         ;BACKSPACE CURSOR
        LD      PCLOW(P2)       ;GET TEST-FAIL ADDRESS
        XPAL    P3              ; FROM I.L. TABLE, PUT IT
        LD      PCHIGH(P2)      ; INTO I.L. PROGRAM COUNTER
        XPAH    P3
        LD      (P3)
        ST      PCHIGH(P2)
        LD      1(P3)
        ST      PCLOW(P2)
        JMP     X10
$MAYBE: XAE                     ;SAVE VALUE (0-25)
        LD      (P1)            ;CHECK FOLLOWING CHAR
        SCL                     ;MUST NOT BE A LETTER
        CAI     'Z'+1           ; OTHERWISE WE'D BE LOOKING
        JP      $OK             ; AT A KEYWORD, NOT A VARIABLE
        SCL
        CAI     'A'-'Z'-1
        JP      $FAIL
$OK:    LDI     H(AESTK)        ;SET P3 TO CURRENT
        XPAH    P3              ; STACK LOCATION
        ILD     LSTK(P2)        ;INCR STACK POINTER
        XPAL    P3
        CCL                     ;DOUBLE VARIABLE INDEX
        LDE
        ADE
        ST      -1(P3)          ;PUT INDEX ON STACK
        LDI     2               ;INCREMENT I.L. PC, SKIPPING
        CCL                     ; OVER TEST-FAIL ADDRESS
        ADD     PCLOW(P2)
        ST      PCLOW(P2)
        LDI     0
        ADD     PCHIGH(P2)
        ST      PCHIGH(P2)
        JMP     X10
 
 
;*************************************
;*    IND - EVALUATE A VARIABLE      *
;*************************************
 
IND:    LDI     H(AESTK)        ;SET P3 TO STACK
        XPAH    P3
        ILD     LSTK(P2)
        XPAL    P3
        LD      -2(P3)          ;GET INDEX OFF TOP
        XAE                     ;PUT INDEX IN E REG
        LD      EREG(P2)        ;GET LOWER 8 BITS
        ST      -2(P3)          ;SAVE ON STACK
        CCL
        LDE                     ;INCREMENT E REG
        ADI     1
        XAE                  
        LD      EREG(P2)        ;GET UPPER 8 BITS
        ST      -1(P3)          ;SAVE ON STACK
X11:    JMP     X10
 
 
;*************************************
;*      RELATIONAL OPERATORS        *
;*************************************
 
EQ:     LDI     1               ;EACH RELATIONAL OPERATOR
        JMP     CMP             ; LOADS A NUMBER USED LATER
NEQ:    LDI     2               ; AS A CASE SELECTOR, AFTER
        JMP     CMP             ; THE TWO OPERANDS ARE COM-
LSS:    LDI     3               ; PARED.  BASED ON THE COM-
        JMP     CMP             ; PARISON, FLAGS ARE SET THAT
LEQ:    LDI     4               ; ARE EQUIVALENT TO THOSE SET
        JMP     CMP             ; BY THE 'CMP' INSTRUCTION IN
GTR:    LDI     5               ; THE PDP-11. THESE PSEUDO-
        JMP     CMP             ; FLAGS ARE USED TO DETERMINE
GEQ:    LDI     6               ; WHETHER THE PARTICULAR
                                ; RELATION IS SATISFIED OR NOT
CMP:    ST      NUM(P2)
        LDI     H(AESTK)        ;SET P3 -> ARITH STACK
        XPAH    P3
        DLD     LSTK(P2)
        DLD     LSTK(P2)
        XPAL    P3
        SCL
        LD      -2(P3)           ;SUBTRACT THE TWO OPERANDS,
        CAD     (P3)             ; STORING RESULT IN LO & HI
        ST      LO(P2)
        LD      -1(P3)
        CAD     1(P3)
        ST      HI(P2)
        XOR     -1(P3)           ;OVERFLOW OCCURS IF SIGNS OF
        XAE                      ; RESULT AND 1ST OPERAND
        LD      -1(P3)           ; DIFFER, AND SIGNS OF THE
        XOR     1(P3)            ; TWO OPERANDS DIFFER
        ANE                      ;BIT 7 EQUIVALENT TO V FLAG
        XOR     HI(P2)           ;BIT 7 EQUIVALENT TO N XOR V
        ST      TEMP(P2)         ;STORE IN TEMP
        LD      HI(P2)           ;DETERMINE IF RESULT WAS ZERO
        OR      LO(P2)
        JZ      SETZ             ;IF RESULT=0, SET Z FLAG
        LDI     080              ; ELSE CLEAR Z FLAG
SETZ:   XRI     080
        XAE                      ;BIT 7 OF EX = Z FLAG
        DLD     NUM(P2)          ;TEST FOR =
        JNZ     NEQ1
        LDE                      ; EQUAL IF Z = 1
        JMP     CMP1
X12:    JMP     X11
NEQ1:   DLD     NUM(P2)          ;TEST FOR <>
        JNZ     LSS1
        LDE                      ; NOT EQUAL IF Z = 0
        XRI     080
        JMP     CMP1
LSS1:   DLD     NUM(P2)          ;TEST FOR <                  
        JNZ     LEQ1
        LD      TEMP(P2)         ; LESS THAN IF (N XOR V)=l
        JMP     CMP1
LEQ1:   DLD     NUM(P2)          ;TEST FOR <=
        JNZ     GTR1
        LDE                      ; LESS THAN OR EQUAL
        OR      TEMP(P2)         ;  IF (Z OR (N XOR V))=l
        JMP     CMP1
GTR1:   DLD     NUM(P2)          ;TEST FOR >
        JNZ     GEQ1
        LDE                      ; GREATER THAN
        OR      TEMP(P2)         ;  IF (Z OR (N XOR V))=0
        XRI     080
        JMP     CMP1
GEQ1:   LD      TEMP(P2)         ;GREATER THAN OR EQUAL
        XRI     080              ; IF (N XOR V)=0
CMP1:   JP      FALSE_           ;IS RELATION SATISFIED?
        LDI     1                ;YES - PUSH 1 ON STACK
        JMP     CMP2
FALSE_: LDI     0                ;NO - PUSH 0 ON STACK
CMP2:   ST      -2(P3)
        LDI     0 
        ST      -1(P3)
        JS      P3,RTN           ;DO AN I.L. RETURN
        JMP     X12
 
 
;*************************************
;*   IF STATEMENT TEST FOR ZERO      *
;*************************************
                                           
CMPR:   LD      LO(P2)          ;GET LOW & HI BYTES OF EXPR.
        OR      HI(P2)          ;TEST IF EXPRESSION IS ZERO
        JZ      FAIL            ;YES - IT IS
        JMP     X12             ;NO - IT ISN'T SO CONTINUE
FAIL:   LD      @1(P1)          ;SKIP TO NEXT LINE IN PROGRAM
        XRI     0D              ; (I.E. TIL NEXT CR)
        JNZ     FAIL
        JS      P3,NXT          ;CALL NXT AND RETURN
X12A:   JMP     X12
 
 
;*************************************
;*        AND, OR, & NOT             *
;*************************************

	.LOCAL 
ANDOP:  LDI     1               ;EACH OPERATION HAS ITS
        JMP     $1              ; OWN CASE SELECTOR.
OROP:   LDI     2   
        JMP     $1
NOTOP:  LDI     3
$1:     ST      NUM(P2)                                        
        LDI     H(AESTK)        ;SET P3 -> ARITH. STACK
        XPAH    P3
        DLD     LSTK(P2)
        DLD     LSTK(P2)
        XPAL    P3
        DLD     NUM(P2)         ;TEST FOR 'AND'
        JNZ     $OR
        LD      1(P3)           ;REPLACE TWO TOP ITEMS ON
        AND     -1(P3)          ; STACK BY THEIR 'AND'
        ST      -1(P3)
        LD      0(P3)
        AND     -2(P3)
        ST      -2(P3)
        JMP     X12A
$OR:    DLD     NUM(P2)         ;TEST FOR 'OR'
        JNZ     $NOT
        LD      1(P3)           ;REPLACE TWO TOP ITEMS ON
        OR      -1(P3)          ; STACK BY THEIR 'OR'
        ST      -1(P3)
        LD      0(P3)
        OR      -2(P3)
        ST      -2(P3)
        JMP     X12A
$NOT:   LD      @1(P3)          ;'NOT' OPERATION
        XRI     0FF
        ST      -1(P3)          ;REPLACE TOP ITEM ON STACK
        LD      @1(P3)          ; BY ITS ONE'S COMPLEMENT
        XRI     0FF
        ST      -1(P3)
        XPAL    P3
        ST      LSTK(P2)        ;STACK POINTER FIXUP
X12B:   JMP     X12A
 
 
;*************************************
;*     EXCHANGE CURSOR WITH RAM      *
;*************************************
 
XCHGP1: LD      P1LOW(P2)       ;THIS ROUTINE IS HANDY WHEN
        XPAL    P1              ; EXECUTING AN 'INPUT' STMT
        ST      P1LOW(P2)       ; IT EXCHANGES THE CURRENT
        LD      P1HIGH(P2)      ; TEXT CURSOR WITH ONE SAVED
        XPAH    P1              ; IN RAM
        ST      P1HIGH(P2)
        XPPC    P3
 
 
;*************************************
;*        CHECK RUN MODE             *
;*************************************
 
CKMODE: LD      RUNMOD(P2)      ;THIS ROUTINE CAUSES AN ERROR
        JZ      CK1             ; IF CURRENTLY IN EDIT MODE
        XPPC    P3
CK1:    LDI     3
E8:     ST      NUM(P2)         ;ERROR IF RUN MODE = 0
        JS      P3,ERR2         ;MINOR KLUGE
 
 
;*************************************
;*      GET HEXADECIMAL NUMBER       *
;*************************************

	.LOCAL 
HEX:    ILD     LSTK(P2)        ;POINT P3 AT ARITH STACK
        ILD     LSTK(P2)
        XPAL    P3
        LDI     H(AESTK)
        XPAH    P3
        LDI      0              ;NUMBER INITIALLY ZERO
        ST      -1(P3)          ;PUT IT ON STACK
        ST      -2(P3)
        ST      NUM(P2)         ;ZERO NUMBER OF DIGITS
$SKIP:  LD      @1(P1)          ;SKIP ANY SPACES
        XRI     ' '
        JZ      $SKIP
        LD      @-1(P1)
$LOOP:  LD      (P1)            ;GET A CHARACTER
        SCL
        CAI     '9'+1           ;CHECK FOR A NUMERIC CHAR
        JP      $LETR
        SCL
        CAI     '0'-'9'-1       ;IF NUMERIC, SHIFT NUMBER
        JP      $ENTER          ; AND ADD NEW HEX DIGIT
        JMP     $END
X12C:   JMP     X12B
$LETR:  SCL                     ;CHECK FOR HEX LETTER
        CAI     'G'-'9'-1
        JP      $END
        SCL
        CAI     'A'-'G'
        JP      $OK
        JMP     $END
$OK:    CCL                     ;ADD 10 TO GET TRUE VALUE
        ADI     10              ; OF LETTER
$ENTER: XAE                     ;NEW DIGIT IN EX REG
        LDI     4               ;SET SHIFT COUNTER
        ST      TEMP(P2)
        ST      NUM(P2)         ;DIGIT COUNT IS NON-ZERO
$SHIFT: LD      -2(P3)          ;SHIFT NUMBER LEFT BY 4
        CCL
        ADD     -2(P3)
        ST      -2(P3)
        LD      -1(P3)
        ADD     -1(P3)
        ST      -1(P3)
        DLD     TEMP(P2)
        JNZ     $SHIFT
        LD      -2(P3)          ;ADD NEW DIGIT
        ORE                     ; INTO NUMBER
        ST      -2(P3)
        LD      @1(P1)          ;ADVANCE THE CURSOR
        JMP     $LOOP           ;GET NEXT CHAR
$END:   LD      NUM(P2)         ;CHECK IF THERE WERE
        JNZ     X12B            ; MORE THAN 0 CHARACTERS
        LDI     5               ;ERROR IF THERE WERE NONE
E8B:    JMP     E8
 
 
;*************************************
;*      TEST FOR NUMBER IN TEXT      *
;*************************************
 
; THIS ROUTINE TESTS FOR A NUMBER IN THE TEXT.  IF NO
; NUMBER IS FOUND, I.L. CONTROL PASSES TO THE ADDRESS
; INDICATED IN THE 'TSTN' INSTRUCTION.  OTHERWISE, THE
; NUMBER IS SCANNED AND PUT ON THE ARITHMETIC STACK,
; WITH I.L. CONTROL PASSING TO THE NEXT INSTRUCTION.

	.LOCAL 
TSTNUM: LD      @1(P1)
        XRI     ' '             ;SKIP OVER ANY SPACES
        JZ      TSTNUM
        LD      @-1(P1)         ;GET FIRST CHAR
        SCL                     ;TEST FOR DIGIT
        CAI     '9'+1
        JP      $ABORT
        SCL
        CAI     '0'-'9'-1
        JP      $1
$ABORT: LD      PCLOW(P2)       ;GET TEST-FAIL ADDRESS
        XPAL    P3              ; FROM I.L. TABLE
        LD      PCHIGH(P2)
        XPAH    P3
        LD      (P3)            ;PUT TEST-FAIL ADDRESS
        ST      PCHIGH(P2)      ; INTO I.L. PC
        LD      1(P3)
        ST      PCLOW(P2)
        JMP     X12C
$RET:   LDI     2               ;SKIP OVER ONE IL INSTRUCTION
        CCL                     ; IF NUMBER IS DONE
        ADD     PCLOW(P2)
        ST      PCLOW(P2)
        LDI     0
        ADD     PCHIGH(P2)
        ST      PCHIGH(P2)
X13:    JMP     X12C
ESA:    JMP     E8B
$1:     XAE                     ;SAVE DIGIT IN EX REG
        LDI     H(AESTK)        ;POINT P3 AT AE STACK
        XPAH    P3
        ILD     LSTK(P2)
        ILD     LSTK(P2)
        XPAL    P3
        LDI     0
        ST      -1(P3)
        LDE
        ST      -2(P3)
$LOOP:  LD      @1(P1)          ;GET NEXT CHAR
        LD      (P1)
        SCL                     ;TEST IF IT IS DIGIT
        CAI     '9'+1
        JP      $RET            ;RETURN IF IT ISN'T
        SCL
        CAI     '0'-'9'-1
        JP      $2
        JMP     $RET
$2:     XAE                     ;SAVE DIGIT
        LD      -1(P3)          ;PUT RESULT IN SCRATCH SPACE
        ST      1(P3)
        LD      -2(P3)
        ST      (P3)
        LDI     2
        ST      TEMP(P2)        ;MULTIPLY RESULT BY 10
$SHIFT: CCL                     ;FIRST MULTIPLY BY 4
        LD      -2(P3)
        ADD     -2(P3)
        ST      -2(P3)
        LD      -1(P3)
        ADD     -1(P3)
        ST      -1(P3)
        ANI     080             ;MAKE SURE NO OVERFLOW
        JNZ     $ERR            ; OCCURRED
        DLD     TEMP(P2)
        JNZ     $SHIFT
        CCL                     ;THEN ADD OLD RESULT,
        LD       -2(P3)         ; SO WE HAVE RESULT * 5
        ADD     (P3)
        ST       -2(P3)
        LD       -1(P3)
        ADD     1(P3)
        ST       -1(P3)
        ANI     080             ;MAKE SURE NO OVERFLOW
        JNZ     $ERR            ; OCCURRED
        CCL                     ;THEN MULTIPLY, BY TWO
        LD      -2(P3)
        ADD     -2(P3)
        ST      -2(P3)
        LD      -1(P3)
        ADD     -1(P3)
        ST      -1(P3)
        ANI     080             ;MAKE SURE NO OVERFLOW
        JNZ     $ERR            ; OCCURRED
        CCL                     ;THEN ADD IN NEW DIGIT
        LDE
        ADD     -2(P3)
        ST      -2(P3)
        LDI     0
        ADD     -1(P3)
        ST      -1(P3)
        JP      $LOOP           ;REPEAT IF NO OVERFLOW
$ERR:   LDI     6
E9:     JMP     ESA             ;ELSE REPORT ERROR
X14:    JMP     X13
 
 
;*************************************
;*    GET LINE FROM TELETYPE         *
;*************************************

	.LOCAL 
GETL:   LDPI    P1,LBUF		;SET P1 TO LBUF
        LDI     0               ;CLEAR NO. OF CHAR
        ST      CHRNUM(P2)
        LDPI    P3,PUTC-1       ;POINT P3 AT PUTC ROUTINE
        LD      RUNMOD(P2)      ;PRINT '? ' IF RUNNING
        JZ      $0              ; (I.E. DURING 'INPUT')
        LDI     '?'
        XPPC     P3
        LDI     ' '
        XPPC    P3
        JMP     $1
$0:     LDI     '>'             ;OTHERWISE PRINT '>'
        XPPC    P3   
$1:     JS      P3,GECO         ;GET CHARACTER
        LDI     L(PUTC)-1       ;POINT P3 AT PUTC AGAIN
        XPAL    P3
        LDE                     ;GET TYPED CHAR
        JZ      $1              ;IGNORE NULLS
        XRI     0A              ;IGNORE LINE FEED
        JZ      $1
        LDE
        XRI     0D              ;CHECK FOR CR
        JZ      $CR
        LDE
        XRI     'O'+010         ;CHECK FOR SHIFT/O
        JZ      $RUB
        LDE                     ;CHECK FOR CTRL/H
        XRI     8
        JZ      $XH
        LDE
        XRI     015             ;CHECK FOR CTRL/U
        JZ      $XU
        LDE
        XRI     3               ;CHECK FOR CTRL/C
        JNZ     $ENTER
        LDI     '^'             ;ECHO CONTROL/C AS ^C
        XPPC    P3
        LDI     'C'
        XPPC    P3
        LDI     14              ;CAUSE A BREAK
        JMP     E9
$XU:    LDI     '^'             ;ECHO CONTROL/U AS ^U
        XPPC    P3
        LDI     'U'
        XPPC    P3
        LDI     0D              ;PRINT CR/LF
        XPPC    P3                                          
        LDI     0A
        XPPC    P3
        JMP     GETL            ;GO GET ANOTHER LINE
X15:    JMP     X14
$ENTER: LDE
        ST      @1(P1)          ;PUT CHAR IN LBUF
        ILD     CHRNUM(P2)      ;INCREMENT CHRNUM
        XRI     72              ;IF=72, LINE FULL
        JNZ     $1
        LDI     0D
        XAE                     ;SAVE CARRIAGE RET
        LDE
        XPPC    P3              ;PRINT IT
        JMP     $CR             ;STORE IT IN LBUF
E10:    JMP     E9                                              
$XH:    LDI     ' '             ;BLANK OUT THE CHARACTER
        XPPC    P3
        LDI     8               ;PRINT ANOTHER BACKSPACE
        XPPC    P3
$RUB:   LD      CHRNUM(P2)
        JZ      $1
        DLD     CHRNUM(P2)      ;ONE LESS CHAR
        LD      @-1(P1)         ;BACKSPACE CURSOR
        JMP     $1
$CR:    LDE
        ST      @1(P1)          ;STORE CR IN LBUF
        LDI     0A              ;PRINT LINE FEED
        XPPC    P3
        LDI     H(LBUF)         ;SET P1 TO BEGIN-
        XPAH    P1              ;  NING OF LBUF
        LDI     L(LBUF)
        XPAL    P1
X16:    JMP     X15
 
 
;*************************************
;*     EVAL -- GET MEMORY CONTENTS   *
;*************************************
 
; THIS ROUTINE IMPLEMENTS THE '@' OPERATOR IN EXPRESSIONS
 
EVAL:   LDI     H(AESTK)
        XPAH    P3
        LD      LSTK(P2)
        XPAL    P3              ;P3 -> ARITH STACK  
        LD      -1(P3)          ;GET ADDR OFF STACK,
        XPAH    P1              ; AND INTO P1,
        XAE                     ; SAVING OLD P1 IN EX & LO
        LD      -2(P3)                                       
        XPAL    P1
        ST      LO(P2)
        LD      0(P1)           ;GET MEMORY CONTENTS,
        ST      -2(P3)          ; SHOVE ONTO STACK
        LDI     0
        ST      -1(P3)          ;HIGH ORDER 8 BITS ZEROED
        LD      LO(P2)
        XPAL    P1              ;RESTORE ORIGINAL P1
        LDE
        XPAH    P1
        JMP     X15
 
 
;*************************************
;*    MOVE -- STORE INTO MEMORY      *
;*************************************
 
; THIS ROUTINE IMPLEMENTS THE STATEMENT:
;    '@' FACTOR '=' REL-EXP
 
MOVE:   LDI     H(AESTK)
        XPAH    P3
        LD      LSTK(P2)
        XPAL    P3              ;P3 -> ARITH STACK
        LD      @-2(P3)         ;GET BYTE TO BE MOVED
        XAE                                                    
        LD      @-1(P3)         ;NOW GET ADDRESS INTO P3
        ST      TEMP(P2)
        LD      @-1(P3)
        XPAL    P3
        ST      LSTK(P2)        ;STACK PTR UPDATED NOW
        LD      TEMP(P2)
        XPAH    P3
        LDE
        ST      0(P3)           ;MOVE THE  BYTE INTO MEMORY
X17:    JMP     X16
Ell:    JMP     E10
 
;*************************************
;*            TEXT EDITOR            *
;*************************************
                                             
;INPUTS TO THIS ROUTINE: POINTER TO LINE BUFFER IN P1LOW &
; P1HIGH.  P1 POINTS TO THE INSERTION POINT IN THE TEXT.
; THE A.E. STACK HAS THE LINE NUMBER ON IT (STACK POINTER
; IS ALREADY POPPED).
 
;EACH LINE IN THE NIBL TEXT IS STORED IN THE FOLLOWING
; FORMAT: TWO BYTES CONTAINING THE LINE NUMBER (IN BINARY,
; HIGH ORDER BYTE FIRST), THEN ONE BYTE CONTAINING THE
; LENGTH OF THE LINE, AND FINALLY THE LINE ITSELF FOLLOWED
; BY A CARRIAGE RETURN.  THE LAST LINE IN THE TEXT IS
; FOLLOWED BY TWO CONSECUTIVE BYTES OF X'FF.

	.LOCAL                      
INSRT:  LDI     H(AESTK)        ;POINT P3 AT AE STACK,
        XPAH    P3              ; WHICH HAS THE LINE #
        LD      LSTK(P2)        ; ON IT
        XPAL    P3
        LD      1(P3)           ;SAVE NEW LINE'S NUMBER
        ST      HILINE(P2)
        LD      0(P3)
        ST      LOLINE(P2)
        LD      P1LOW(P2)       ;PUT POINTER TO LBUF INTO P3
        XPAL    P3
        LD      P1HIGH(P2)
        XPAH    P3
        LDI     4               ;INITIALLY LENGTH OF NEW LINE
        ST      CHRNUM(P2)      ; = 4. ADD 1 TO LENGTH FOR
$1:     LD      @1(P3)          ; EACH CHAR IN LINE UP TO, BUT
        XRI     0D              ; NOT INCLUDING, CARR. RETURN
        JZ      $2
        ILD     CHRNUM(P2)
        JMP     $1
$2:     LD      CHRNUM(P2)      ;IF LENGTH STILL 4, WE'LL DELETE
        XRI     4               ; A LINE, SO SET LENGTH = 0
        JNZ     $3
        ST      CHRNUM(P2)
$3:     LD      CHRNUM(P2)      ;PUT NEW LINE LENGTH IN EX
        XAE
        LD      LABLHI(P2)      ;IS NEW LINE REPLACING OLD?
        JP      $4              ;YES - DO REPLACE
        ANI     07F             ;NO - WE'LL INSERT LINE HERE,
        ST      LABLHI(P2)      ; WHERE FNDLBL GOT US
        JMP     $MOVE           ;BUT FIRST MAKE ROOM
$4:     LD      @3(P1)          ;SKIP LINE # AND LENGTH
        LDE                     ;EX, NOW HOLDING NEW LINE
        CCL                     ; LENGTH, WILL SOON HOLD
        ADI     -4              ; DISPLACEMENT OF LINES
        XAE                     ; TO BE MOVED
$5:     LD      @1(P1)          ;SUBTRACT 1 FROM DISPLACEMENT
        XRI     0D              ; FOR EACH CHAR IN LINE BEING
        JZ      $MOVE           ; REPLACED
        LDE
        CCL
        ADI     -1
        XAE
        JMP     $5
X19:    JMP     X17
E12:    JMP     Ell
$MOVE:  LDE                     ;IF DISPLACEMENT AND LENGTH
        OR      CHRNUM(P2)      ; OF NEW LINE ARE 0, RETURN
        JZ      X19
        LDI     L(DOSTAK)       ;CLEAR SOME STACKS
        ST      DOPTR(P2)
        LDI     L(SBRSTK)
        ST      SBRPTR(P2)
        LDI     L(FORSTK)                                    
        ST      FORPTR(P2)
        LDE
        JZ      $ADD            ;DON'T NEED TO MOVE LINES
        JP      $UP             ;SKIP IF DISPLACEMENT POSITIVE
$DOWN:  LD      0(P1)           ;NEGATIVE DISPLACEMENT:
        ST      EREG(P1)        ;DO;
        LD      @1(P1)          ;   M(P1+DISP) = M(P1);
        JP      $DOWN           ;   P1 = P1+1;
        LD      0(P1)           ;UNTIL M(P1)<0 & M(P1-1)<0;
        JP      $DOWN
        ST      EREG(P1)        ;M(P1+DISP) = M(P1);
        JMP     $ADD
$UP:    LD      -2(P1)          ;POSITIVE DISPLACEMENT:
        ST      TEMP(P2)        ;FLAG BEGINNING OF MOVE WITH
        LDI     -1              ; A -1 FOLLOWED BY 80, WHICH
        ST      -2(P1)          ; CAN NEVER APPEAR IN A
        LDI     80              ; NIBL TEXT
        ST      -1(P1)
$UP1:   LD      @1(P1)          ;ADVANCE P1 TO END OF TEXT
        JP      $UP1
        LD      0(P1)
        JP      $UP1
        XPAH    P1              ;SAVE P1 IN LO, HI
        ST      HI(P2)
        XPAH    P1   
        XPAL    P1
        ST      LO(P2)
        XPAL    P1
        LD      LO(P2)          ;ADD DISPLACEMENT TO
        CCL                     ; VALUE OF P1, TO CHECK
        ADE                     ; WHETHER WE'RE OUT OF
        LDI     0               ; RAM FOR USER'S PROGRAM
        ADD     HI(P2)
        XOR     HI(P2)
        ANI     0F0
        JZ      $UP2
        LDI     0               ;IF OUT OF RAM, CHANGE
        XAE                     ; DISPLACEMENT TO ZERO
$UP2:   LDI     -1
$UP3:   ST      EREG(P1)        ;MOVE TEXT UP UNTIL WE REACH
        LD      @-1(P1)         ; THE FLAGS SET ABOVE
        JP      $UP3
        LD      1(P1)
        XRI     80
        JZ      $UP4
        LD      0(P1)
        JMP     $UP3
$UP4:   LD      TEMP(P2)        ;RESTORE THE FLAGGED LOCATION
        ST      0(P1)           ; TO THEIR ORIGINAL VALUES
        LDI     0D
        ST      1(P1)
        LDE                     ;IF DISPLACEMENT = 0, WE'RE
        JNZ     $ADD            ; OUT OF RAM, SO REPORT ERROR
        LDI     2
E12A:   JMP     E12
$ADD:   LD      CHRNUM(P2)      ;INSERT NEW LINE
X19A:   JZ      X19             ; UNLESS LENGTH IS ZERO
        LD      P1LOW(P2)       ;POINT P1 AT LINE BUFFER
        XPAL    P1
        LD      P1HIGH(P2)
        XPAH    P1
        LD      LABLLO(P2)      ;POINT P3 AT INSERTION PLACE
        XPAL    P3
        LD      LABLHI(P2)
        XPAH    P3
        LD      HILINE(P2)      ;PUT LINE NUMBER INTO TEXT
        ST      @1(P3)
        LD      LOLINE(P2)
        ST      @1(P3)
        LD      CHRNUM(P2)      ;STORE LINE LENGTH IN TEXT
        ST      @1(P3)
$ADD1:  LD      @1(P1)          ;PUT REST OF CHARS
        ST      @1(P3)          ; (INCLUDING OR) INTO TEXT
        XRI     0D
        JNZ     $ADD1
        JMP     X19A            ;RETURN
X20:    JS      P3,EXECIL
E13:    JMP     E12A
 
 
;************************************
;*       POP ARITHMETIC STACK       *
;************************************
 
POPAE:  DLD     LSTK(P2)        ;THIS ROUTINE POP THE A.E.
        DLD     LSTK(P2)        ; STACK, AND PUTS THE RESULT
        XPAL    P3              ; INTO LO(P2) AND HI(P2)
        LDI     H(AESTK)
        XPAH    P3
        LD      (P3)
        ST      LO(P2)
        LD      1(P3)
        ST      HI(P2)
        JMP     X20
 
 
;*************************************
;*              UNTIL                *
;*************************************

	.LOCAL 
UNTIL:  LD      DOPTR(P2)       ;CHECK FOR DO-STACK UNDERFLOW
        XAE
        LDE 
        XRI     L(DOSTAK)
        JNZ     $1
        LDI     15
        JMP     E13                                         
$1:     LD      LO(P2)          ;CHECK FOR EXPRESSION = 0
        OR      HI(P2)
        JZ      $REDO           ;IF ZERO, REPEAT DO-LOOP
        DLD     DOPTR(P2)       ;ELSE POP SAVE STACK
        DLD     DOPTR(P2)
        JMP     X20             ;CONTINUE TO NEXT STMT
$REDO:  LDE                     ;POINT P3 AT DO-STACK
        XPAL    P3
        LDI     H(DOSTAK)
        XPAH    P3
        LD      -1(P3)          ;LOAD P1 FROM DO STACK
        XPAH    P1
        LD      -2(P3)
        XPAL    P1              ;CURSOR NOW POINTS TO FIRST
        JMP     X20             ; STATEMENT OF DO-LOOP

 
;*************************************
;*     STORE INTO STATUS REGISTER    *
;*************************************
 
; THIS ROUTINE IMPLEMENTS THE STATEMENT:
;     'STAT' '=' REL-EXP
 
MOVESR:  LD      LO(P2)         ;LOW BYTE GOES TO STATUS
         ANI     0F7            ; BUT WITH IEN BIT CLEARED
         CAS
X21:     JMP     X20
E14:     JMP     E13
 
 
;*************************************
;*         STAT FUNCTION             *
;*************************************
 
STATUS: LDI     H(AESTK)
        XPAH    P3              ;POINT P3 AT AE STACK
        ILD     LSTK(P2)
        ILD     LSTK(P2)
        XPAL    P3
        CSA
        ST      -2(P3)          ;STATUS REG IS LOW BYTE
        LDI     0
        ST      -1(P3)          ;ZERO IS HIGH BYTE
        JMP     X21
 
 
;*************************************
;*    MACHINE LANGUAGE SUBROUTINE    *
;*************************************
 
; THIS ROUTINE IMPLEMENTS THE 'LINK' STATEMENT
 
CALLML: LD      HI(P2)          ;GET HIGH BYTE OF ADDRESS
        XPAH    P3
        LD      LO(P2)          ;GET LOW BYTE
        XPAL    P3              ;P3 -> USER'S ROUTINE
        LD      @-1(P3)         ;CORRECT P3
        XPPC    P3              ;CALL ROUTINE (PRAY IT WORKS)
        LDPI    P2,VARS         ;RESTORE RAM POINTER
        JMP     X21             ;RETURN
 
 
;*************************************
;*        SAVE DO LOOP ADDRESS       *
;*************************************
 
; THIS ROUTINE IMPLEMENTS THE 'DO' STATEMENT.

	.LOCAL 
SAVEDO: LD      DOPTR(P2)       ;CHECK FOR STACK OVERFLOW
        XRI     L(FORSTK)
        JNZ     $1
        LDI     10
E15:    JMP     E14
$1:     ILD     DOPTR(P2)
        ILD     DOPTR(P2)
        XPAL    P3
        LDI     H(DOSTAK)
        XPAH    P3              ;P3 -> TOP OF DO STACK
        XPAH    P1              ;SAVE CURSOR ON THE STACK
        ST      -1(P3)
        XPAH    P1
        XPAL    P1
        ST      -2(P3)
        XPAL    P1
X22:    JMP     X21
 
 
;*************************************
;*        TOP OF RAM FUNCTION        *
;*************************************

	.LOCAL 
TOP:    LD      TEMP2(P2)       ;SET P3 TO POINT TO
        XPAH    P3              ; START OF NIBL TEXT
        LD      TEMP3(P2)
        XPAL    P3
$0:     LD      (P3)            ;HAVE WE HIT END OF TEXT?
        JP      $1              ;NO - SKIP TO NEXT LINE
        JMP     $2              ;YES - PUT CURSOR ON STACK
$1:     LD      2(P3)           ;GET LENGTH OF LINE
        XAE
        LD      @EREG(P3)       ;SKIP TO NEXT LINE
        JMP     $0              ;GO CHECK FOR EOF
$2:     LD      @2(P3)          ;P3 := P3 + 2
        ILD     LSTK(P2)        ;SET P3 TO STACK, SAVING
        ILD     LSTK(P2)        ; OLD P3 (WHICH CONTAINS TOP)
        XPAL    P3              ; ON IT SOMEHOW
        XAE
        LDI     H(AESTK)
        XPAH    P3
        ST      -1(P3)
        LDE
        ST      -2(P3)
        JMP     X22
 
 
;*************************************
;*       SKIP TO NEXT NIBL LINE      *
;*************************************
 
IGNORE: LD      @1(P1)          ;SCAN TIL WE'RE PAST
        XRI     0D              ; CARRIAGE RETURN
        JNZ     IGNORE
        XPPC    P3		;YES - RETURN
 
 
;*************************************
;*          MODULO FUNCTION          *
;*************************************
 
MODULO: LD      LSTK(P2)        ;THIS ROUTINE MUST BE
        XPAL    P3              ; IMMEDIATELY AFTER A
        LDI     H(AESTK)        ; DIVIDE TO WORK CORRECTLY
        XPAH    P3
        LD      3(P3)           ;GET LOW BYTE OF REMAINDER
        ST      -2(P3)          ;PUT ON STACK
        LD      2(P3)           ;GET HIGH BYTE OF REMAINDER
        ST      -1(P3)          ;PUT ON STACK
X23:    JMP     X22
E16:    JMP     E15
 
 
;*************************************
;*          RANDOM FUNCTION          *
;*************************************

	.LOCAL 
RANDOM: LDI     8               ;LOOP COUNTER FOR MULTIPLY
        ST      NUM(P2)
        LD      RNDX(P2)
        XAE
        LD      RNDY(P2)
        ST      TEMP2(P2)
$LOOP:  LD      RNDX(P2)        ;MULTIPLY THE SEEDS BY 9
        CCL
        ADE
        XAE
        LD      RNDY(P2)
        CCL
        ADD     TEMP2(P2)
        ST      RNDY(P2)
        DLD     NUM(P2)
        JNZ     $LOOP
        LDE                     ;ADD 7 TO SEEDS
        CCL
        ADI     7
        XAE
        LD      RNDY(P2)
        CCL
        ADI     7
        RR
        ST      RNDY(P2)
        ILD     RNDF(P2)        ;HAVE WE GONE THROUGH
        JZ      $1              ; 256 GENERATIONS?
        LDE                     ;IF SO, SKIP GENERATING
        ST      RNDX(P2)        ; THE NEW RNDX
$1:     LD      LSTK(P2)        ;START MESSING WITH THE STACK
        XPAL    P3
        LDI     H(AESTK)
        XPAH    P3
        LDI     1               ;FIRST PUT 1 ON STACK
        ST      (P3)
        LDI     0
        ST      1(P3)
        LD      -2(P3)          ;PUT EXPR2 ON STACK
        ST      2(P3)
        LD      -1(P3)
        ST      3(P3)
        LD      -4(P3)          ;PUT EXPR1 ON STACK
        ST      4(P3)
        LD      -3(P3)
        ST      5(P3)
        LD      RNDY(P2)        ;PUT RANDOM # ON STACK
        ST      -2(P3)
        LD      RNDX(P2)
        XRI     0FF
        ANI     07F
        ST      -1(P3)
        LD      @6(P3)          ;ADD 6 TO STACK POINTER
        XPAL    P3
        ST      LSTK(P2)
X24:    JMP     X23
E16A:   JMP     E16
 
 
;*************************************
;*     PUSH 1 ON ARITHMETIC STACK    *
;*************************************
 
LIT1:   ILD     LSTK(P2)
        ILD     LSTK(P2)
        XPAL    P3
        LDI     H(AESTK)
        XPAH    P3
        LDI     0
        ST      -1(P3)
        LDI     1
        ST      -2(P3)
        JMP     X24
 
 
;*************************************
;*      FOR-LOOP INITIALIZATION      *
;*************************************

	.LOCAL 
SAVFOR: LD      FORPTR(P2)      ;CHECK FOR FOR STACK
        XRI     L(PCSTAK)       ; OVERFLOW
        JNZ     $1
        LDI     10
E17:    JMP     E16A
$1:     XRI     L(PCSTAK)
        XPAL    P1              ;POINT P1 AT FOR STACK
        ST      P1LOW(P2)       ; SAVING OLD P1
        LDI     H(FORSTK)
        XPAH    P1
        ST      P1HIGH(P2)
        LD      LSTK(P2)        ;POINT P3 AT AE STACK
        XPAL    P3
        LDI     H(AESTK)
        XPAH    P3
        LD      -7(P3)          ;GET VARIABLE INDEX
        ST      @1(P1)          ;SAVE ON FOR-STACK
        LD      -4(P3)          ;GET L(LIMIT)
        ST      @1(P1)          ;SAVE
        LD      -3(P3)          ;GET H(LIMIT)
        ST      @1(P1)          ;SAVE
        LD      -2(P3)          ;GET L(STEP)
        ST      @1(P1)          ;SAVE
        LD      -1(P3)          ;GET H(STEP)
        ST      @1(P1)          ;SAVE
        LD      P1LOW(P2)       ;GET L(P1)
        ST      @1(P1)          ;SAVE
        LD      P1HIGH(P2)      ;GET H(P1)
        ST      @1(P1)          ;SAVE
        XPAH    P1              ;RESTORE OLD P1
        LD      P1LOW(P2)
        XPAL    P1
        ST      FORPTR(P2)      ;UPDATE POR STACK PTR
        LD      @-4(P3)
        XPAL    P3
        ST      LSTK(P2)        ;UPDATE AE STACK PTR
X25:    JMP     X24
 
 
;*************************************
;*     FIRST PART OF 'NEXT VAR'      *
;*************************************

	.LOCAL 
NEXTV:  LD      FORPTR(P2)      ;POINT P1 AT FOR STACK,
        XRI     L(FORSTK)       ; CHECKING FOR UNDERFLOW
        JNZ     $1
        LDI     11              ;REPORT ERROR
        JMP     E17
$1:     XRI     L(FORSTK)
        XPAL    P1
        ST      P1LOW(P2)       ;SAVE OLD P1
        LDI     H(FORSTK)
        XPAH    P1
        ST      P1HIGH(P2) 
        LD      LSTK(P2)        ;POINT P3 AT AE STACK
        XPAL    P3
        LDI     H(AESTK)
        XPAH    P3
        LD      @-1(P3)         ;GET VARIABLE INDEX
        XOR     -7(P1)          ;COMPARE WITH INDEX
        JZ      $10             ; ON FOR STACK: ERROR
        LDI     12              ; IF NOT EQUAL
E18:    JMP     E17
$10:    XOR     -7(P1)          ;RESTORE INDEX
        XAE                     ;SAVE IN EREG
        LD      EREG(P2)        ;GET L(VARIABLE)
        CCL
        ADD     -4(P1)          ;ADD L(STEP)
        ST      EREG(P2)        ;STORE IN VARIABLE
        ST      (P3)            ; AND ON STACK
        LD      @1(P2)          ;INCREMENT RAM PTR
        LD      EREG(P2)        ;GET H(VARIABLE)
        ADD     -3(P1)          ;ADD H(STEP)
        ST      EREG(P2)        ;STORE IN VARIABLE
        ST      1(P3)           ; AND ON STACK
        LD      @-1(P2)         ;RESTORE RAM POINTER
        LD      -6(P1)          ;GET L(LIMIT)
        ST      2(P3)           ;PUT ON STACK
        LD      -5(P1)          ;GET H(LIMIT)
        ST      3(P3)           ;PUT ON STACK
        LD      -3(P1)          ;GET H(STEP)
        JP      $2              ;IF NEGATIVE, INVERT
        LDI     4               ; ITEMS ON A.E. STACK
        ST      NUM(P2)         ;NUM = LOOP COUNTER
$LOOP:  LD      @1(P3)          ;GET BYTE FROM STACK
        XRI     0FF             ;INVERT IT
        ST      -1(P3)          ;PUT BACK ON STACK
        DLD     NUM(P2)         ;DO UNTIL NUM = 0
        JNZ     $LOOP
        JMP     $3
$2:     LD      @4(P3)          ;UPDATE AE STACK POINTER
$3:     XPAL    P3
        ST      LSTK(P2)
        LD      P1LOW(P2)       ; RESTORE OLD P1
        XPAL    P1
        LD      P1HIGH(P2)
        XPAH    P1
X26:    JMP     X25
 
 
;*************************************
;*     SECOND PART OF 'NEXT VAR'     *
;*************************************
 
NEXTV1: LD      LO(P2)          ;IS FOR-LOOP OVER WITH?
        JZ      $REDO           ;NO - REPEAT LOOP
        LD      FORPTR(P2)      ;YES - POP FOR-STACK
        CCL
        ADI     -7
        ST      FORPTR(P2)
        XPPC    P3              ;RETURN TO I.L. INTERPRETER
$REDO:  LD      FORPTR(P2)      ;POINT P3 AT FOR STACK
        XPAL    P3                                            
        LDI     H(FORSTK)
        XPAH    P3                              
        LD      -1(P3)          ;GET OLD P1 OFF STACK
        XPAH    P1
        LD      -2(P3)
        XPAL    P1
        JMP     X26
E19:    JMP     E18
;************************************
;*      PRINT MEMORY AS STRING      *
;************************************
 
; THIS ROUTINE IMPLEMENTS THE STATEMENT:
;     'PRINT' '$' FACTOR
 
	.LOCAL
PSTRNG: LD      HI(P2)          ;POINT P1 AT STRING TO PRINT
        XPAH    P1
        LD      LO(P2)
        XPAL    P1
        LDPI    P3,PUTC-1       ;POINT P3 AT PUTC ROUTINE
$1:     LD      @1(P1)          ;GET A CHARACTER
        XRI     0D              ;IS IT A CARRIAGE RETURN?
        JZ      X26             ;YES - WE'RE DONE
        XRI     0D              ;NO - PRINT THE CHARACTER
        XPPC    P3
        CSA                     ;MAKE SURE NO ONE IS
        ANI     020             ; TYPING ON THE TTY
        JNZ     $1              ; BEFORE REPEATING LOOP
        JMP     X26
 
 
;************************************
;*        INPUT A STRING            *
;************************************
 
; THIS ROUTINE IMPLEMENTS THE STATEMENT:
;     'INPUT' '$' FACTOR
 
ISTRNG: LD      HI(P2)          ;GET ADDRESS TO STORE THE
        XPAH    P3              ; STRING, PUT IT INTO P3
        LD      LO(P2)
        XPAL    P3
$2:     LD      @1(P1)          ;GET A BYTE FROM LINE BUFFER
        ST      @1(P3)          ;PUT IT IN SPECIFIED LOCATION
        XRI     0D              ;DO UNTIL CHAR = CARR. RETURN
        JNZ     $2
X27:    JMP     X26
 
 
;************************************
;*   STRING CONSTANT ASSIGNMENT     *
;************************************
 
; THIS ROUTINE IMPLEMENTS THE STATEMENT:
;     '$' FACTOR '=' STRING
         
	.LOCAL
PUTSTR: LD      LO(P2)          ;GET ADDRESS TO STORE STRING,
        XPAL    P3              ; PUT IT INTO P3
        LD      HI(P2)
        XPAH    P3
$LOOP:  LD      @1(P1)          ;GET A BYTE FROM STRING
        XRI     '"'             ;CHECK FOR END OF STRING
        JZ      $END
        XRI     '"' ! 0D        ;MAKE SURE THERE'S NO CR
        JNZ     $1
        LDI     7
        JMP     E19             ;ERROR IF CARRIAGE RETURN
$1:     XRI     0D              ;RESTORE CHARACTER
        ST      @1(P3)          ;PUT IN SPECIFIED LOCATION
        JMP     $LOOP           ;GET NEXT CHARACTER
$END:   LDI     0D              ;APPEND CARRIAGE RETURN
        ST      (P3)            ; TO STRING
        JMP     X27
 
 
;************************************
;*           MOVE STRING            *
;************************************
 
; THIS ROUTINE IMPLEMENTS THE STATEMENT:
;     '$' FACTOR '=' '$' FACTOR

	.LOCAL 
MOVSTR: LD      LSTK(P2)        ;POINT P3 AT A.E. STACK
        XPAL    P3
        LDI     H(AESTK)
        XPAH    P3
        LD      @-1(P3)         ;GET ADDRESS OF SOURCE STRING
        XPAH    P1              ; INTO P1
        LD      @-1(P3)
        XPAL    P1
        LD      @-1(P3)         ;GET ADDRESS OF DESTINATION
        XAE                     ; STRING INTO P3
        LD      @-1(P3)
        XPAL    P3
        ST      LSTK(P2)        ;UPDATE STACK POINTER
        LDE
        XPAH    P3
$LOOP:  LD      @1(P1)          ;GET A SOURCE CHARACTER
        ST      @1(P3)          ;SEND IT TO DESTINATION
        XRI     0D              ;REPEAT UNTIL CARRIAGE RET.
        JZ      X27
        CSA                     ; OR KEYBOARD INTERRUPT
        ANI     020
        JNZ     $LOOP
        JMP     X27                                           
 
 
;************************************
;*     PUT PAGE NUMBER ON STACK     *
;************************************
 
PUTPGE: ILD     LSTK(P2)
        ILD     LSTK(P2)
        XPAL    P3
        LDI     H(AESTK)
        XPAH    P3
        LD      PAGE(P2)
        ST      -2(P3)
        LDI     0
        ST      -1(P3)
        JMP     X27
 
 
;************************************
;*        ASSIGN NEW PAGE           *
;************************************
 
	.LOCAL
NUPAGE: LD      LO(P2)          ;GET PAGE # FROM STACK,
        ANI     7               ; GET THE LOW 3 BITS
        JNZ     $0              ;PAGE 0 BECOMES PAGE 1
        LDI     1
$0:     ST      PAGE(P2)
        XPPC    P3              ;RETURN

 
;*************************************
;*         FIND START OF PAGE        *
;*************************************
 
; THIS ROUTINE COMPUTES THE START OF THE CURRENT TEXT PAGE,
; STORING THE ADDRESS IN TEMP2(P2) [THE HIGH BYTE], AND
; TEMP3(P2) [THE LOW BYTE].
 
FNDPGE:  LD      PAGE(P2)
         XRI     1              ;SPECIAL CASE IS PAGE 1, BUT
         JNZ     $1             ; OTHERS ARE CONVENTIONAL
         LDI     H(PGM)         ;PAGE 1 STARTS AT 'PGM'
         ST      TEMP2(P2)
         LDI     L(PGM)
         ST      TEMP3(P2)
         XPPC    P3             ;RETURN
$1:      XRI     1              ;RESTORE PAGE #
         XAE                    ;SAVE IT
         LDI     4              ;LOOP COUNTER = 4
         ST      NUM(P2)
$LOOP:   LDE                    ;MULTIPLY PAGE# BY 16
         CCL                                 
         ADE
         XAE
         DLD     NUM(P2)
         JNZ     $LOOP
         LDE
         ST      TEMP2(P2)      ;TEMP2 HAS HIGH BYTE
         LDI     2              ; OF ADDRESS NOW
         ST      TEMP3(P2)      ;LOW BYTE IS ALWAYS 2
         XPPC    P3
;************************************
;*      MOVE CURSOR TO NEW PAGE     *
;************************************
 
CHPAGE: LD      TEMP2(P2)       ;PUT START OF PAGE
        XPAH    P1              ; INTO P1.  THIS ROUTINE
        LD      TEMP3(P2)       ; MUST BE CALLED RIGHT
        XPAL    P1              ; AFTER 'FNDPGE'
        XPPC    P3              ;RETURN
 
 
;************************************
;*      DETERMINE CURRENT PAGE      *
;************************************
 
DETPGE: XPAH    P1              ;CURRENT PAGE IS HIGH
        XAE                     ; PART OF CURSOR DIVIDED
        LDE                     ; BY 16
        XPAH    P1
        LDE
        SR
        SR
        SR
        SR
        ST      PAGE(P2)
        XPPC    P3              ;RETURN
 
 
;************************************
;*         CLEAR CURRENT PAGE       *
;************************************
 
NEWPGM: LD      TEMP2(P2)       ;POINT P1 AT CURRENT PAGE
        XPAH    P1
        LD      TEMP3(P2)
        XPAL    P1
        LDI     0D              ;PUT DUMMY END-OF-LINE
        ST      -1(P1)          ; JUST BEFORE TEXT
        LDI     -1              ;PUT -1 AT START OF TEXT
        ST      (P1)
        ST      1(P1)
        XPPC    P3              ;RETURN
 
 
;*************************************
;*      FIND LINE NUMBER IN TEXT     *
;*************************************
 
;INPUTS: THE START OF THE CURRENT PAGE IN TEMP2 AND TEMPS,
;        THE LINE NUMBER TO LOOK FOR IN LO AND HI.
;OUPUTS: THE ADDRESS OF THE FIRST LINE IN THE NIBL TEXT
;        WHOSE LINE NUMBER IS GREATER THAN OR EQUAL TO THE
;        NUMBER IN HI AND LO, RETURNED IN P1 AND ALSO IN
;        IN THE RAM VARIABLES LABLLO AND LABLHI.  THE SIGN
;        BIT OF LABLHI IS SET IF EXACT LINE IS NOT FOUND.

	.LOCAL        
FNDLBL: LD      TEMP2(P2)       ;POINT P1 AT START OF TEXT
        XPAH    P1
        LD      TEMP3(P2)
        XPAL    P1
$1:     LD      (P1)            ;HAVE WE HIT END OF TEXT?
        XRI     0FF
        JP      $2              ;YES - STOP LOOKING
        SCL                     ;NO - COMPARE LINE NUMBERS
        LD      1(P1)           ; BY SUBTRACTING
        CAD     LO(P2)
        LD      0(P1)
        CAD     HI(P2)          ;IS TEXT LINE # >= LINE #?
        JP      $2              ;YES - STOP LOOKING.
        LD      2(P1)           ;NO - TRY NEXT LINE IN TEXT
        XAE
        LD      @EREG(P1)       ; SKIP LENGTH OF LINE
        JMP     $1
$2:     XPAL    P1              ;SAVE ADDRESS OF FOUND LINE
        ST      LABLLO(P2)      ; IN LABLHI AND LABLLO
        XPAL    P1
        XPAH    P1
        ST      LABLHI(P2)
        XPAH    P1
        LD      LO(P2)          ;WAS THERE AN EXACT MATCH?
        XOR     1(P1)
        JNZ     $3
        LD      HI(P2)
        XOR     0(P1)
        JNZ     $3              ;NO - FLAG THE ADDRESS
        XPPC    P3              ;YES - RETURN NORMALLY
$3:     LD      LABLHI(P2)      ;SET SIGN BIT OF HIGH PART
        ORI     080             ; OF ADDRESS TO INDICATE
        ST      LABLHI(P2)      ; INEXACT MATCH OF LINE #'S
        XPPC    P3
        .PAGE   ' I. L. MACROS'

;***********************************
;*          I. L. MACROS           *
;***********************************

	.LOCAL
 
$TSTBIT =	TSTBIT*256
$CALBIT =	CALBIT*256
$JMPBIT =	JMPBIT*256

	.MACRO  TST,FAIL,A,B
        .DBYTE	$TSTBIT!FAIL
         IFB    B
	.BYTE	A!080 
         ELSE
         DB     A 
	.BYTE	B!080
         ENDIF
        .ENDM
 
	.MACRO	TSTCR,FAIL
	.DBYTE	$TSTBIT!FAIL
	.BYTE	0D!080
	.ENDM
 
	.MACRO	TSTV,FAIL
	.ADDR	TSTVAR
	.DBYTE	FAIL
        .ENDM
 
	.MACRO	TSTN,FAIL
	.ADDR	TSTNUM
	.DBYTE	FAIL
	.ENDM
 
	.MACRO	JUMP,ADR
	.DBYTE	$JMPBIT!ADR
	.ENDM
 
	.MACRO	CALL,ADR
	.DBYTE	$CALBIT!ADR
	.ENDM

	.MACRO	DO,ADR
         IFNB    ADR 
	.ADDR	ADR
         SHIFT
         DO      ALLARGS
         ENDIF
	.ENDM

	.PAGE	' I. L. TABLE'         
 
;*************************************
;*            I. L. TABLE            *
;*************************************
 
START:	DO	NLINE
PROMPT: DO	GETL
	TSTCR	PRMPT1
        JUMP    PROMPT
PRMPT1: TSTN    LIST
        DO      FNDPGE,XCHGP1,POPAE,FNDLBL,INSRT
        JUMP    PROMPT
 
LIST:   TST     RUN,"LIS",'T'
        DO      FNDPGE
        TSTN    LIST1
        DO      POPAE,FNDLBL
        JUMP    LIST2
LIST1:  DO      CHPAGE
LIST2:  DO      LST
LIST3:  CALL    PRNUM
        DO      LST3
        JUMP    START

RUN:    TST     CLR,"RU",'N'
        DO      DONE
BEGIN:  DO      FNDPGE,CHPAGE,STRT,NXT

CLR:    TST     NEW,"CLEA",'R'
        DO      DONE,CLEAR,NXT

NEW:    TST     STMT,"NE",'W'
        TSTN    DFAULT
        JUMP    NEW1
DFAULT: DO      LIT1
NEW1:   DO      DONE,POPAE,NUPAGE,FNDPGE,NEWPGM,NXT

STMT:   TST     LET,"LE",'T'
LET:    TSTV    AT
        TST     SYNTAX,'='
        CALL    RELEXP
        DO      STORE,DONE,NXT
AT:     TST     IF,'@'
        CALL    FACTOR
        TST     SYNTAX,'='
        CALL    RELEXP
        DO      MOVE,DONE,NXT
 
IF:     TST     UNT,"I",'F'
        CALL    RELEXP
        TST     IF1,"THE",'N'
IF1:    DO      POPAE,CMPR
        JUMP    STMT
UNT:    TST     DOSTMT,"UNTI",'L'
        DO      CKMODE
        CALL    RELEXP
        DO      DONE,POPAE,UNTIL,DETPGE,NXT
 
DOSTMT: TST     GOTO,"D",'O'
        DO      CKMODE,DONE,SAVEDO,NXT
GOTO:   TST     RETURN,"G",'O'
        TST     GOSUB,"T",'O'
        CALL    RELEXP
        DO      DONE
        JUMP    GO1
GOSUB:  TST     SYNTAX,"SU",'B'
        CALL    RELEXP
        DO      DONE,SAV
GO1:    DO      FNDPGE, POPAE,FNDLBL,XFER,NXT
 
RETURN: TST     NEXT,"RETUR",'N'
        DO      DONE,RSTR,DETPGE,NXT

NEXT:   TST     FOR,"NEX",'T'
        DO      CKMODE
        TSTV    SYNTAX
        DO      DONE,NEXTV
        CALL    GTROP
        DO      POPAE, NEXTV1,DETPGE,NXT
 
FOR:    TST     STAT,"FO",'R'
        DO      CKMODE
        TSTV    SYNTAX
        TST     SYNTAX,'='
        CALL    RELEXP
        TST     SYNTAX,"T",'O'
        CALL    RELEXP
        TST     FOR1,"STE",'P'
        CALL    RELEXP
        JUMP    FOR2
FOR1:   DO      LIT1
FOR2:   DO      DONE,SAVFOR,STORE,NXT
 
STAT:   TST     PGE,"STA",'T'
        TST     SYNTAX,'='
        CALL    RELEXP
        DO      POPAE,MOVESR
        DO      DONE,NXT
 
PGE:    TST     DOLLAR,"PAG",'E'
        TST     SYNTAX,'='
        CALL    RELEXP
        DO      DONE,POPAE,NUPAGE,FNDPGE,CHPAGE,NXT
 
DOLLAR: TST     PRINT,'$'
        CALL    FACTOR
        TST     SYNTAX,'='
        TST     DOLR1,'"'
        DO      POPAE,PUTSTR
        JUMP    DOLR2
DOLR1:  TST     SYNTAX,'$'
        CALL    FACTOR
        DO      XCHGP1,MOVSTR,XCHGP1
DOLR2:  DO      DONE,NXT
 
PRINT:  TST     INPUT,"P",'R'
        TST     PR1,"IN",'T'
PR1:    TST     PR2,'"'
        DO      PRS
        JUMP    COMMA
PR2:    TST     PR3,'$'
        CALL    FACTOR
        DO      XCHGP1,POPAE,PSTRNG,XCHGP1
        JUMP    COMMA
PR3:    CALL    RELEXP
        CALL    PRNUM
COMMA:  TST     PR4,','
        JUMP    PR1
PR4:    TST     PR5,';'
        JUMP    PR6
PR5:    DO      NLINE
PR6:    DO      DONE,NXT

INPUT:  TST     END,"INPU",'T'
        DO      CKMODE
        TSTV    IN2
        DO      XCHGP1,GETL
IN1:    CALL    RELEXP
        DO      STORE,XCHGP1
        TST     IN3,','
        TSTV    SYNTAX
        DO      XCHGP1
        TST     SYNTAX,','
        JUMP    IN1
IN2:    TST     SYNTAX,'$'
        CALL    FACTOR
        DO      XCHGP1,GETL,POPAE,ISTRNG,XCHGP1
IN3:    DO      DONE,NXT
 
END:    TST     ML,"EN",'D'
        DO      DONE,BREAK
 
ML:     TST     REM,"LIN",'K'
        CALL    RELEXP
        DO      DONE,XCHGP1,POPAE,CALLML,XCHGP1,NXT           
                                                              
REM:    TST     SYNTAX,"RE",'M'
        DO      IGNORE,NXT
SYNTAX: DO      ERR
ERRNUM: CALL    PRNUM
        DO      FIN
 
; NOTE: EACH RELATIONAL OPERATOR (EQ, LEQ, ETC.) DOES AN
; AUTOMATIC 'RTN' (THIS SAVES VALUABLE BYTES!)
 
RELEXP: CALL    EXPR
        TST     REL1,'='
        CALL    EXPR
        DO      EQ
REL1:   TST     REL4,'<'
        TST     REL2,'='
        CALL    EXPR
        DO      LEQ
REL2:   TST     REL3,'>'
        CALL    EXPR
        DO      NEQ
REL3:   CALL    EXPR
        DO      LSS
REL4:   TST     RETEXP,'>'
        TST     REL5,'='
        CALL    EXPR
        DO      GEQ
REL5:   CALL    EXPR
GTROP:  DO      GTR
 
EXPR:   TST     EX1,'-'
        CALL    TERM
        DO      NEG
        JUMP    EX3
EX1:    TST     EX2,'+'
EX2:    CALL    TERM
EX3:    TST     EX4,'+'
        CALL    TERM
        DO      ADD
        JUMP    EX3
EX4:    TST     EX5,'-'
        CALL    TERM
        DO      SUB
        JUMP    EX3
EX5:    TST     RETEXP,"O",'R'
        CALL    TERM
        DO      OROP
        JUMP    EX3
RETEXP: DO      RTN
 
TERM:   CALL    FACTOR
T1:     TST     T2,'*'
        CALL    FACTOR
        DO      MUL
        JUMP    T1
T2:     TST     T3,'/'
        CALL    FACTOR
        DO      DIV  
        JUMP    T1
T3:     TST     RETEXP,"AN",'D'
        CALL    FACTOR
        DO      ANDOP
        JUMP    T1
 
FACTOR: TSTV    F1
        DO      IND,RTN
F1:     TSTN    F2
        DO      RTN
F2:     TST     F3,'#'
        DO      HEX,RTN
F3:     TST     F4,'('
        CALL    RELEXP
        TST     SYNTAX,')'
        DO      RTN
F4:     TST     F5,'@'
        CALL    FACTOR
        DO      EVAL,RTN
F5:     TST     F6,"NO",'T'
        CALL    FACTOR
        DO      NOTOP,RTN
F6:     TST     F7,"STA",'T'
        DO      STATUS,RTN
F7:     TST     F8,"TO",'P'
        DO      FNDPGE,TOP,RTN
F8:     TST     F9,"MO",'D'
        CALL    DOUBLE
        DO      DIV,MODULO,RTN
F9:     TST     F10,"RN",'D'
        CALL    DOUBLE
        DO      RANDOM,SUB,ADD,DIV,MODULO,ADD,RTN
F10:    TST     SYNTAX,"PAG",'E'
        DO      PUTPGE,RTN
 
DOUBLE: TST     SYNTAX,'('
        CALL    RELEXP
        TST     SYNTAX,','
        CALL    RELEXP
        TST     SYNTAX,')'
        DO      RTN
 
PRNUM:  DO      XCHGP1,PRN
PRNUM1: DO      DIV,PRN1,XCHGP1,RTN
	.PAGE	'ERROR MESSAGES'
 
;*************************************
;*           ERROR MESSAGES          *
;*************************************
 
	.MACRO	MESSAGE,A,B
          DB  A
	.BYTE	B!080
	.ENDM

MESGS:  MESSAGE " ERRO",'R'     ; 1
        MESSAGE "ARE",'A'       ; 2
        MESSAGE "STM",'T'       ; 3
        MESSAGE "CHA",'R'       ; 4
        MESSAGE "SNT",'X'       ; 5
        MESSAGE "VAL",'U'       ; 6
        MESSAGE "END",'"'       ; 7
        MESSAGE "NOG",'O'       ; 8
        MESSAGE "RTR",'N'       ; 9
        MESSAGE "NES",'T'       ; 10
        MESSAGE "NEX",'T'       ; 11
        MESSAGE "FO" ,'R'       ; 12
        MESSAGE "DIV",'0'       ; 13
        MESSAGE "BR" ,'K'       ; 14
        MESSAGE "UNT",'L'       ; 15
        .PAGE	' TELETYPE ROUTINES'
 
;*************************************
;*     GET CHARACTER AND ECHO IT     *
;*************************************
;
	.LOCAL
GECO:   LDI     8               ;SET COUNT = 8
        ST      NUM(P2)
        CSA                     ;SET READER RELAY
        ORI     2
        CAS
$1:     CSA                     ;WAIT FOR START BIT
        ANI     020
        JNZ     $1		;NOT FOUND
        LDI     0A7             ;DELAY 1/2 BIT TIME
        DLY     0
        CSA                     ;IS START BIT STILL THERE?
        ANI     020
        JNZ     $1		;NO
        CSA                     ;SEND START BIT
        ANI     %2              ;RESET READER RELAY
        ORI     1
        CAS
$2:	LDI     045             ;DELAY 1 BIT TIME
        DLY     01
        CSA                     ;GET BIT (SENSEB)
        ANI     020
        JZ      $3
        LDI     1
        JMP     $4
$3:	LDI     0
        JNZ     $4
$4:	ST      TEMP(P2)        ;SAVE BIT VALUE (0 OR 1)
        RRL                     ;ROTATE INTO LINK
        XAE
        SRL                     ;SHIFT INTO CHARACTER
        XAE                     ;RETURN CHAR TO E
        CSA                     ;ECHO BIT TO OUTPUT
        ORI     1
        XOR     TEMP(P2)
        CAS
        DLD     NUM(P2)         ;DECREMENT BIT COUNT
        JNZ     $2		;LOOP UNTIL 0
	LDI	045		;DELAY BIT TIME FOR MSB
	DLY	1
        CSA                     ;SET STOP BIT
        ANI     0FE
        CAS
        DLY     4               ;DELAY APPROX. 1 BIT TIME
        LDE                     ;AC HAS INPUT CHARACTER
        ANI     07F
        XAE
        LDE
        XPPC    P3              ;RETURN
        JMP     GECO
;*************************************
;*     PRINT CHARACTER AT TTY        *
;*************************************
 
PUTC:   XAE
        LDI     025             ;DELAY ALMOST
        DLY     3               ;3 BIT TIMES
        CSA                     ;SET OUTPUT BIT TO LOGIC 0
        ORI     1               ; FOR START BIT (NOTE INVERSION)
        CAS
        LDI     9               ;INITIALIZE BIT COUNT
        ST      TEMP3(P2)
PUTC1:  LDI     050             ;DELAY 1 BIT TIME
        DLY     1    
        DLD     TEMP3(P2)       ;DECREMENT BIT COUNT
        JZ      PUTC2
        LDE                     ;PREPARE NEXT BIT
        ANI     1
        ST      TEMP2(P2)
        XAE			;SHIFT DATA RIGHT 1 BIT
        SR
        XAE
        CSA                     ;SET UP OUTPUT BIT
        ORI    1
        XOR    TEMP2(P2)
        CAS                     ;PUT BIT INTO TTY
        JMP    PUTC1
PUTC2:  CSA                     ;SET STOP BIT
        ANI    0FE
        CAS
        XPPC   P3		;RETURN
        JMP    PUTC
	.END   0
