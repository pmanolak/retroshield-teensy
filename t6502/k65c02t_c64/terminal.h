#ifndef _TERMINAL_H
#define _TERMINAL_H

////////////////////////////////////////////////////////////////////
// VT100 Functions for Screen
// Code from Michael Steil
////////////////////////////////////////////////////////////////////

enum {
    COLOR_WHITE,
    COLOR_RED,
    COLOR_GREEN,
    COLOR_BLUE,
    COLOR_BLACK,
    COLOR_PURPLE,
    COLOR_YELLOW,
    COLOR_CYAN,
    COLOR_ORANGE,
    COLOR_BROWN,
    COLOR_LTRED,
    COLOR_GREY1,
    COLOR_GREY2,
    COLOR_LTGREEN,
    COLOR_LTBLUE,
    COLOR_GREY3,
};

void clear_screen() {
  Serial.write("\033[2J\033[;H");
}

void up_cursor() {
  Serial.write("\033[A");
}

void down_cursor() {
  Serial.write("\033[B");
}

void left_cursor() {
  Serial.write("\033[D");
}

void right_cursor() {
  Serial.write("\033[C");
}

void set_color(int c)
{
  /* TODO */
}

void move_cursor(int x, int y) {
  // Serial.print("\033[%d;%df", y, x);

  char tmp[40];
  sprintf(tmp, "\033[%d;%df", y, x);
  Serial.write(tmp);  
}

bool quote_mode = 0;
bool text_mode = false;
int column = 0;
int columns =0;

void screen_bsout(char a)
{
  if (quote_mode) {
    if (a == '"' || a == '\n' || a == '\r') {
      quote_mode = 0;
    }
    Serial.write(a);
  } else {
    switch (a) {
      case 5:
        set_color(COLOR_WHITE);
        break;
      case 10:
        break;
      case 13:
        Serial.write(13);
        Serial.write(10);
        column = 0;
        break;
      case 14: // TEXT
        text_mode = true;
        break;
      case 17: // CSR DOWN
        down_cursor();
        break;
      case 19: // CSR HOME
        move_cursor(0, 0);
        break;
      case 28:
        set_color(COLOR_RED);
        break;
      case 29: // CSR RIGHT
        right_cursor();
        break;
      case 30:
        set_color(COLOR_GREEN);
        break;
      case 31:
        set_color(COLOR_BLUE);
        break;
      case 129:
        set_color(COLOR_ORANGE);
        break;
      case 142: // GRAPHICS
        text_mode = false;
        break;
      case 144:
        set_color(COLOR_BLACK);
        break;
      case 145: // CSR UP
        up_cursor();
        break;
      case 147: // clear screen
#ifndef NO_CLRHOME
        clear_screen();
#endif
        break;
      case 149:
        set_color(COLOR_BROWN);
        break;
      case 150:
        set_color(COLOR_LTRED);
        break;
      case 151:
        set_color(COLOR_GREY1);
        break;
      case 152:
        set_color(COLOR_GREY2);
        break;
      case 153:
        set_color(COLOR_LTGREEN);
        break;
      case 154:
        set_color(COLOR_LTBLUE);
        break;
      case 155:
        set_color(COLOR_GREY3);
        break;
      case 156:
        set_color(COLOR_PURPLE);
        break;
      case 158:
        set_color(COLOR_YELLOW);
        break;
      case 159:
        set_color(COLOR_CYAN);
        break;
      case 157: // CSR LEFT
        left_cursor();
        break;
      case '"':
        quote_mode = 1;
        // fallthrough
      default: {
//        printf("<%i(%i)>", a, column);
        unsigned char c = a;
        if (text_mode) {
          if (c >= 0x41 && c <= 0x5a) {
            c += 0x20;
          } else if (c >= 0x61 && c <= 0x7a) {
            c -= 0x20;
          } else if (c >= 0xc1 && c <= 0xda) {
            c -= 0x80;
          }
        }
        Serial.write(c);
        if (c == '\b') {
          if (column) {
            column--;
          }
        } else {
          column++;
          if (column == columns) {
            Serial.write('\n');
            column = 0;
          }
        }
      }
    }
  }
  // fflush(stdout);
}



////////////////////////////////////////////////////////////////////
// Custom Hack at 0xC000
////////////////////////////////////////////////////////////////////
// Magic address 0xCFFF.
//   Write to 0xCFFF -> Byte goes to Arduino Serial.
//   Read from 0xCFFF <-> Character or 0x00 if no character
//
const unsigned char custom_bin[] = {
  0x8D, 0xFF, 0xCF, 0x6C, 0x26, 0x03,
  // 0xAD, 0xFF, 0xCF, 0xF0, 0xFB, 0x8D, 0xFF, 0xCF, 0x60   // linear Serial mode
  0x48, 0x8A, 0x48, 0xAD, 0xFF, 0xCF, 0xF0, 0x03, 0x20, 0x35, 0xEB,
  0x68, 0xAA, 0x68, 0x4C, 0x48, 0xFF
};

// ^^^ Interrupt Vector to handle keyboard correctly.
//
//48       pha      
//8a       txa      
//48       pha      
//ad ff cf lda $cfff
//f0 03    beq $c00b
//20 35 eb jsr $eb35
//68       pla      
//aa       tax      
//68       pla      
//4c 48 ff jmp $ff48



void init_C64()
{
    // Standard OUT to Arduino Serial
  kernal_bin[ (0xFFD2 - KERNAL_START) ] = 0x4C;
  kernal_bin[ (0xFFD3 - KERNAL_START) ] = 0x00;
  kernal_bin[ (0xFFD4 - KERNAL_START) ] = 0xC0;

    // Reroute Interrupt vector to handle keyboard
  kernal_bin[ (0xFFFE - KERNAL_START) ] = 0x06;
  kernal_bin[ (0xFFFF - KERNAL_START) ] = 0xC0;

    // Speed up memory check (check every page)
  kernal_bin[ (0xFD83 - KERNAL_START) ] = 0x4C;
  kernal_bin[ (0xFD84 - KERNAL_START) ] = 0x6C;
  kernal_bin[ (0xFD85 - KERNAL_START) ] = 0xFD;
    
//    if ( uP_ADDR == 0xFFD2 )
//    {
//      DATA_OUT = ( 0x4C );
//    }
//    else
//    if ( uP_ADDR == 0xFFD3 )
//    {
//      DATA_OUT = ( 0x00 );
//    }
//    else
//    if ( uP_ADDR == 0xFFD4 )
//    {
//      DATA_OUT = ( 0xC0 );
//    }
//    else

//    // Reroute Interrupt vector to handle keyboard
//    if ( uP_ADDR == 0xFFFE )
//    {
//      DATA_OUT = ( 0x06 );
//    }
//    else
//    if ( uP_ADDR == 0xFFFF )
//    {
//      DATA_OUT = ( 0xC0 );
//    }
//    else

//    // Speed up memory check (check every page)
//    if ( uP_ADDR == 0xFD83 )
//    {
//      DATA_OUT = ( 0x4C );
//    }
//    else
//    if ( uP_ADDR == 0xFD84 )
//    {
//      DATA_OUT = ( 0x6C );
//    }
//    else
//    if ( uP_ADDR == 0xFD85 )
//    {
//      DATA_OUT = ( 0xFD );
//    }
//    else  
}

#endif // _TERMINAL_H