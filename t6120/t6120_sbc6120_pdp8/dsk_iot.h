#ifndef _DSK_IOT_H
#define _DSK_IOT_H

  // Code samples are from 
  // David Betz (https://github.com/dbetz/pico_pdp8)
  // Ian Schofield (https://groups.google.com/g/pidp-8/c/r1a_KFR5VEQ)

  // MIT License

  // Copyright (c) 2022 David Betz

  // Permission is hereby granted, free of charge, to any person obtaining a copy
  // of this software and associated documentation files (the "Software"), to deal
  // in the Software without restriction, including without limitation the rights
  // to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  // copies of the Software, and to permit persons to whom the Software is
  // furnished to do so, subject to the following conditions:

  // The above copyright notice and this permission notice shall be included in all
  // copies or substantial portions of the Software.

  // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  // IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  // FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  // AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  // LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  // OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  // SOFTWARE.

#include <SPI.h>
#include <SD.h>

const char *dsk_iot_fname       = "df32_os8_4p.dsk";
const char *dsk_iot_fname_orig  = "df32_os8_4p_orig.dsk";

bool dsk_iot_finit    = false;
File dsk_iot_dataFile = NULL;

static unsigned int dskrg   = 0x00, 
                    dskmem  = 0x00, 
                    dskfl   = 0x00, 
                    tm      = 0x00;
static unsigned int dskad   = 0x00;


// Erturk: Comment left from David Betz's files.
// We are using function names, not the 9p protocol.
//
// based on code from simple test program for 9p access to host files
// Written by Eric R. Smith, Total Spectrum Software Inc.
// Released into the public domain.
//

int dsk_seek(word diskaddr)
{
    Serial.print("seek 0x"); Serial.println(diskaddr, HEX);
    return dsk_iot_finit ? dsk_iot_dataFile.seek( (uint64_t) diskaddr *2) : -1;
    // we use 2bytes/word.
}

size_t dsk_read(word memaddr, size_t size)
{
    Serial.print("read #"); Serial.print(size); Serial.print(" to 0x"); Serial.println(memaddr, HEX);
    // return dsk_iot_finit ? dsk_iot_dataFile.read(buffer, size) : -1;
    //                                           ^^ Warning: Possible buffer overrun !!
    word dd;

    if (dsk_iot_finit)
    {
      while (size)
      {
        dd  = dsk_iot_dataFile.read();
        dd |= dsk_iot_dataFile.read() << 8;
        write_memory(memaddr, dd);
        memaddr = (memaddr++) & 07777;  // (wrap at 32KB)
        size--;
      }

      return size;      // should be 0 if all done.
    }
    else
      return -1;
}

size_t dsk_write(word memaddr, size_t size)
{
    Serial.print("write #"); Serial.print(size); Serial.print(" from 0x"); Serial.println(memaddr, HEX);
    // return dsk_iot_finit ? dsk_iot_dataFile.write(buffer, size) : -1;
    //                                            ^^ Warning: Possible buffer overrun !!    
    word dd;
    bool no_err = true;

    if (dsk_iot_finit)
    {
      while (size && no_err)
      {
        dd = read_memory(memaddr);
        no_err  = dsk_iot_dataFile.write(  dd     & 0xFF);
        no_err |= dsk_iot_dataFile.write( (dd>>8) & 0xFF);
        memaddr = (memaddr++) & 07777;  // (wrap at 32KB)
        size--;
      }
      return size;      // should be 0 if all done.
    }
    else
      return -1;
}


void dsk_init()
{
  // Check for OS8 image file on SDCARD
  dsk_iot_finit = false;

  if (SD_Card_Present)
  {
    // Serial.println("\nSDCard: Checking for DF32 image!");

    bool do_you_want_to_refresh_os_image = false;

    if (do_you_want_to_refresh_os_image = false) // Refresh disk image
    {
      const unsigned int  BUFFER_SIZE = SECTOR_SIZE*4;
      byte                bfr[BUFFER_SIZE];
      unsigned int        bytes_read;

      // Serial.println("\nSDCard: Card present!");
      Serial.println("\nSDCard: Restoring DF32 image from original !");
      Serial.flush();

      SD.remove(dsk_iot_fname);

      // Open files
      File src  = SD.open(dsk_iot_fname_orig, FILE_READ);
      File dest = SD.open(dsk_iot_fname,      FILE_WRITE);

      src.seek(0);
      dest.seek(0);

      while( bytes_read = src.read(bfr, BUFFER_SIZE))
      {
        dest.write(bfr, bytes_read);
        Serial.write("#");
      }
      dest.flush();

      dest.close();
      src.close();
      Serial.println("");
    }

    // open the file.
    dsk_iot_dataFile = SD.open(dsk_iot_fname, FILE_WRITE);

    if (dsk_iot_dataFile) {
      dsk_iot_finit = true;
      Serial.println("DSK_IOT: OS8 Image");
      Serial.print("- DF32 Image Size: 0x"); Serial.println(dsk_iot_dataFile.size(), HEX);
      // delay(1500);

      // dsk_iot_dataFile.close();
    }  
    // if the file can't be open, ignore disk:
    else {
      Serial.println("DSK_IOT: Can not open OS8 disk image.");
      // dataFile.close();
      dsk_iot_finit = false;
      dsk_iot_dataFile = NULL;
    } 
  }      
}


// #include <stdint.h>

// #include "nano8.h"

// static unsigned int dskrg, dskmem, dskfl, tm, i;
// static unsigned int dskad;
// static unsigned int tmp;
// static uint8_t  *p;

// void dsk_iot()
// {
//     switch (inst & 0777) {
//     case 0601:
//         dskad = dskfl = 0;
//         break;
//     case 0605:
//     case 0603:
//         i = (dskrg & 070) << 9;
//         dskmem = ((mem[07751] + 1) & 07777) | i; /* mem */
//         tm = (dskrg & 03700) << 6;
//         dskad = (acc & 07777) + tm; /* dsk */
//         i = 010000 - mem[07750];
//         p = (uint8_t *)&mem[dskmem];
//         dsk_seek(dskad * 2);
//         if (inst & 2) {
//             /*read */
//             //digitalWrite(R_LED, LOW);
//             tmp = dsk_read(p, i * 2);
//             //digitalWrite(R_LED, HIGH);
//             //Serial.printf("Read:%o>%o:%o,%d Len:%d\r\n", dskad, dskmem, dskrg, i, tmp);
//         }
//         else {
//             //digitalWrite(R_LED, LOW);
//             tmp = dsk_write(p, i * 2);
//             //digitalWrite(R_LED, HIGH);
//             //Serial.printf("Write:%o>%o:%o,%d Len:%d\r\n", dskad, dskmem, dskrg, i, tmp);
//         }
//         dskfl = 1;
//         mem[07751] = 0;
//         mem[07750] = 0;
//         acc = acc & 010000;
//         break;
//     case 0611:
//         dskrg = 0;
//         break;
//     case 0615:
//         dskrg = (acc & 07777); /* register */
//         break;
//     case 0616:
//         acc = (acc & 010000) | dskrg;
//         break;
//     case 0626:
//         acc = (acc & 010000) + (dskad & 07777);
//         break;
//     case 0622:
//         if (dskfl) pc++;
//         break;
//     case 0612:
//         acc = acc & 010000;
//     case 0621:
//         pc++; /* No error */
//         break;
//     }
// }

void dsk_clear()
{
    dskfl = 0;
}


#endif
