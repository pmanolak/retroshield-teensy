#ifndef _PORTMAP_H
#define _PORTMAP_H

////////////////////////////////////////////////////////////////////
// Arduino Port Mapping to Teensy GPIO Numbers.
////////////////////////////////////////////////////////////////////
#define MEGA_PD7  (24)
#define MEGA_PG0  (13)
#define MEGA_PG1  (16)
#define MEGA_PG2  (17)
#define MEGA_PB0  (28)
#define MEGA_PB1  (39)
#define MEGA_PB2  (29)
#define MEGA_PB3  (30)

#define MEGA_PC7  (27)
#define MEGA_PC6  (26)
#define MEGA_PC5  (4)
#define MEGA_PC4  (3)
#define MEGA_PC3  (38)
#define MEGA_PC2  (37)
#define MEGA_PC1  (36)
#define MEGA_PC0  (35)

#define MEGA_PL7  (5)
#define MEGA_PL6  (21)
#define MEGA_PL5  (20)
#define MEGA_PL4  (6)
#define MEGA_PL3  (8)
#define MEGA_PL2  (7)
#define MEGA_PL1  (14)
#define MEGA_PL0  (2)

#define MEGA_PA7  (12)
#define MEGA_PA6  (11)
#define MEGA_PA5  (25)
#define MEGA_PA4  (10)
#define MEGA_PA3  (9)
#define MEGA_PA2  (23)
#define MEGA_PA1  (22)
#define MEGA_PA0  (15)

////////////////////////////////////////////////////////////////////
// 6120 Processor Control
////////////////////////////////////////////////////////////////////

/* Digital Pin Assignments */

#define uP_READ_N     MEGA_PA0

#define uP_WRITE_N    MEGA_PA1
#define uP_SKIP_N     MEGA_PG0
#define uP_MEMSEL_N   MEGA_PA2
#define uP_IOCLR_N    MEGA_PA3
#define uP_LXDAR_N    MEGA_PA4
#define uP_LXPAR_N    MEGA_PA6
#define uP_LXMAR_N    MEGA_PA5
#define uP_RESET_N    MEGA_PD7
#define uP_DATAF_N    MEGA_PA7
#define uP_INTGNT_N   MEGA_PG2

#define uP_INTREQ_N   MEGA_PG1
#define uP_CPREQ_N    MEGA_PB3
#define uP_EMA2       MEGA_PC4
#define uP_C1N_EMA1   MEGA_PC5
#define uP_C0N_EMA0   MEGA_PC6
#define uP_CLK        MEGA_PC7


// ##################################################

// This table is global because Teensy needs pinMode's
// initially to setup the ports. Otherwise direct
// accesses don't work.
//

byte DXpinTable[] = {
  // Rev B
  // D11(LSB) .. D0 (MSB)
  MEGA_PL0,MEGA_PL1,MEGA_PL2,MEGA_PL3,MEGA_PL4,MEGA_PL5,MEGA_PL6,MEGA_PL7,
  MEGA_PC0,MEGA_PC1,MEGA_PC2,MEGA_PC3
};

void configure_PINMODE_ADDR()
{
  for (unsigned int i=0; i<sizeof(DXpinTable); i++)
  {
    pinMode(DXpinTable[i],INPUT);
  } 
}

// Repeated for template-able purposes...
void configure_PINMODE_DATA()
{
  for (unsigned int i=0; i<sizeof(DXpinTable); i++)
  {
    pinMode(DXpinTable[i],INPUT);
  } 
}
// ##################################################
// Low level bus access functions
//   for teensy 3.5, 3.6 and 4.1
// ##################################################
//
// #define GPIO?_PDOR    (*(volatile uint32_t *)0x400FF0C0) // Port Data Output Register
// #define GPIO?_PSOR    (*(volatile uint32_t *)0x400FF0C4) // Port Set Output Register
// #define GPIO?_PCOR    (*(volatile uint32_t *)0x400FF0C8) // Port Clear Output Register
// #define GPIO?_PTOR    (*(volatile uint32_t *)0x400FF0CC) // Port Toggle Output Register
// #define GPIO?_PDIR    (*(volatile uint32_t *)0x400FF0D0) // Port Data Input Register
// #define GPIO?_PDDR    (*(volatile uint32_t *)0x400FF0D4) // Port Data Direction Register

#define CLK_HIGH()  digitalWriteFast(uP_CLK, LOW)
#define CLK_LOW()   digitalWriteFast(uP_CLK, HIGH)
// !!! Note to myself - CLK inverted on PCB.

// ##################################################
// 12bit address/databus
// DX11..DX08 = PB3.. PB0 = PTB19..PTB16
// DX07..DX00 = PL7.. PL0 = PTD07..PTD00
//// ##################################################

// ##################################################
#if (ARDUINO_TEENSY35 || ARDUINO_TEENSY36)
// ##################################################

#define xDATA_DIR_IN()    { GPIOC_PDDR = (GPIOC_PDDR & 0xFFFFF0FF); GPIOD_PDDR = (GPIOD_PDDR & 0xFFFFFF00); }
#define xDATA_DIR_OUT()   { GPIOC_PDDR = (GPIOC_PDDR | 0x00000F00); GPIOD_PDDR = (GPIOD_PDDR | 0x000000FF); }
#define SET_DATA_OUT(b)   { GPIOC_PDOR = (GPIOC_PDOR & 0xFFFFF0FF) | (b & 0x0F00); GPIOD_PDOR = (GPIOD_PDOR & 0xFFFFFF00) | (b & 0x00FF); }
#define xDATA_IN()        (                                  ((word) (GPIOC_PDIR & 0x0F00)) | ((word) (GPIOD_PDIR & 0x00FF)) )
#define xADDR_IN()        ( ((word) (GPIOA_PDIR & 0x7000)) | ((word) (GPIOC_PDIR & 0x0F00)) | ((word) (GPIOD_PDIR & 0x00FF)) )

// ##################################################
#elif (ARDUINO_TEENSY41)
// ##################################################

// Teensy 4.1 has different port/pin assignments compared to Teeny 3.5 & 3.6
// so we have to do bit shuffling to construct address and data buses.

// Teensy 4.1's 600Mhz seems to compensate for the delays by executing other
// sections of the code faster.

inline __attribute__((always_inline))
void xDATA_DIR_IN()
{
  // for (byte i=0; i<sizeof(DXpinTable); i++)
  // {
  //   pinMode(DXpinTable[i],INPUT);
  // } 

  // Unroll the loop
  pinMode(MEGA_PL0, INPUT);
  pinMode(MEGA_PL1, INPUT);
  pinMode(MEGA_PL2, INPUT);
  pinMode(MEGA_PL3, INPUT);
  pinMode(MEGA_PL4, INPUT);
  pinMode(MEGA_PL5, INPUT);
  pinMode(MEGA_PL6, INPUT);
  pinMode(MEGA_PL7, INPUT);
  pinMode(MEGA_PC0, INPUT);
  pinMode(MEGA_PC1, INPUT);
  pinMode(MEGA_PC2, INPUT);
  pinMode(MEGA_PC3, INPUT);
}

inline __attribute__((always_inline))
void xDATA_DIR_OUT()
{
  // for (byte i=0; i<sizeof(DXpinTable); i++)
  // {
  //   pinMode(DXpinTable[i],OUTPUT);
  // } 

  // Unroll the loop
  pinMode(MEGA_PL0, OUTPUT);
  pinMode(MEGA_PL1, OUTPUT);
  pinMode(MEGA_PL2, OUTPUT);
  pinMode(MEGA_PL3, OUTPUT);
  pinMode(MEGA_PL4, OUTPUT);
  pinMode(MEGA_PL5, OUTPUT);
  pinMode(MEGA_PL6, OUTPUT);
  pinMode(MEGA_PL7, OUTPUT);
  pinMode(MEGA_PC0, OUTPUT);
  pinMode(MEGA_PC1, OUTPUT);
  pinMode(MEGA_PC2, OUTPUT);
  pinMode(MEGA_PC3, OUTPUT);

}

inline __attribute__((always_inline))
void SET_DATA_OUT(word b)
{
  // for (byte i=0; i<sizeof(DXpinTable); i++)
  // {
  //   digitalWrite(DXpinTable[i], (b & 1));
  //   b = b >> 1;
  // } 

  // Unroll the loop
  // MEGA_PL0,MEGA_PL1,MEGA_PL2,MEGA_PL3,MEGA_PL4,MEGA_PL5,MEGA_PL6,MEGA_PL7
  // MEGA_PC0,MEGA_PC1,MEGA_PC2,MEGA_PC3

  digitalWriteFast(MEGA_PL0, b & 1);
  b = b >> 1;
  digitalWriteFast(MEGA_PL1, b & 1);
  b = b >> 1;
  digitalWriteFast(MEGA_PL2, b & 1);
  b = b >> 1;
  digitalWriteFast(MEGA_PL3, b & 1);
  b = b >> 1;
  digitalWriteFast(MEGA_PL4, b & 1);
  b = b >> 1;
  digitalWriteFast(MEGA_PL5, b & 1);
  b = b >> 1;
  digitalWriteFast(MEGA_PL6, b & 1);
  b = b >> 1;
  digitalWriteFast(MEGA_PL7, b & 1);
  b = b >> 1;
  digitalWriteFast(MEGA_PC0, b & 1);
  b = b >> 1;
  digitalWriteFast(MEGA_PC1, b & 1);
  b = b >> 1;
  digitalWriteFast(MEGA_PC2, b & 1);
  b = b >> 1;
  digitalWriteFast(MEGA_PC3, b & 1);

  DELAY_FOR_BUFFER();       // wait for TXB0108 to stabilize

}

inline __attribute__((always_inline))
word xDATA_IN()
{
  word b = 0;

  // for (byte i=0; i<sizeof(DXpinTable); i++)
  // {
  //   if (digitalRead(DXpinTable[i]) )
  //     b = b | (1<<i);
  // } 

  // Unroll the loop

  b = b | digitalReadFast(MEGA_PC3);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PC2);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PC1);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PC0);
  b = b << 1;

  b = b | digitalReadFast(MEGA_PL7);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PL6);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PL5);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PL4);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PL3);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PL2);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PL1);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PL0);

  return b;
}


inline __attribute__((always_inline))
word xADDR_IN()
{
  word b = 0;

  // b = 
  //   (digitalReadFast(uP_C0N_EMA0) << 14) |        // Address combines EMA0,EMA1,EMA2,DX0..DX11
  //   (digitalReadFast(uP_C1N_EMA1) << 13) |
  //   (digitalReadFast(uP_EMA2)     << 12) |
  //   xDATA_IN();

  // Unroll the loop

  b = b | digitalReadFast(uP_C0N_EMA0);
  b = b << 1;
  b = b | digitalReadFast(uP_C1N_EMA1);
  b = b << 1;
  b = b | digitalReadFast(uP_EMA2);
  b = b << 1;

  b = b | digitalReadFast(MEGA_PC3);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PC2);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PC1);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PC0);
  b = b << 1;

  b = b | digitalReadFast(MEGA_PL7);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PL6);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PL5);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PL4);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PL3);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PL2);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PL1);
  b = b << 1;
  b = b | digitalReadFast(MEGA_PL0);

  return b;
}

// ##################################################
#endif
// ##################################################


// ##################################################
// Fix level shifter problems for C0,C1,SKIP
// ##################################################
#define FORCE_C0   28
#define FORCE_C1   39
#define FORCE_SKIP 29

inline __attribute__((always_inline))
void disable_FORCE()
{
  digitalWriteFast(FORCE_C0,    LOW);
  digitalWriteFast(FORCE_C1,    LOW);
  digitalWriteFast(FORCE_SKIP,  LOW);
}

inline __attribute__((always_inline))
void drive_C0_N(bool hl)
{
  digitalWriteFast(FORCE_C0,    !hl);        // extra pull-down strength
  digitalWriteFast(uP_C0N_EMA0,  hl);
}

inline __attribute__((always_inline))
void drive_C1_N(bool hl)
{
  digitalWriteFast(FORCE_C1,    !hl);        // extra pull-down strength
  digitalWriteFast(uP_C1N_EMA1,  hl);
}

inline __attribute__((always_inline))
void drive_SKIP_N(bool hl)
{
  digitalWriteFast(FORCE_SKIP, !hl);        // extra pull-down strength
  digitalWriteFast(uP_SKIP_N,   hl);
}


// ##################################################
// Teensy Version
// ##################################################
void print_teensy_version()
{
#if (ARDUINO_TEENSY35)
  Serial.println("Teensy:     3.5");
#elif (ARDUINO_TEENSY36)
  Serial.println("Teensy:     3.6");
#elif (ARDUINO_TEENSY41)
  Serial.println("Teensy:     4.1");
#endif
}

#endif    // _PORTMAP_H



// Reference
//
// #define GPIO?_PDOR    (*(volatile uint32_t *)0x400FF0C0) // Port Data Output Register
// #define GPIO?_PSOR    (*(volatile uint32_t *)0x400FF0C4) // Port Set Output Register
// #define GPIO?_PCOR    (*(volatile uint32_t *)0x400FF0C8) // Port Clear Output Register
// #define GPIO?_PTOR    (*(volatile uint32_t *)0x400FF0CC) // Port Toggle Output Register
// #define GPIO?_PDIR    (*(volatile uint32_t *)0x400FF0D0) // Port Data Input Register
// #define GPIO?_PDDR    (*(volatile uint32_t *)0x400FF0D4) // Port Data Direction Register
//