Follow these steps to prepare the SDCard for OS8.

1) copy all *.ide files onto the sdcard root directory.
   Insert sdcard into Teensy.
2) Teensy will detect and print message about disk image.
3) Type B in SBC6120 monitor code to boot OS8.
4) At OS8 prompt ".", enter "SET TTY NO PAUSE" to disable annoying pauses.
5) Start w/ "DIR SYS:" or "DIR IDA1:" to list files.
6) Type "BASIC". Then OLD or NEW, then filename to play w/ basic.


RetroShield 6120 currently emulates SBC6120 IDE. The *.ide files generated from OS8 images following the instructions in SBC6120 manual. This work was done by Will and Steve, as credited in corresponding *_notes.txt files.

IOT updates to emulate real PDP-8 drives is in progress.





Log:
===========
2024-0403: Using SBC6120 disk image from www.grc.com

. Moved initial disk image to archive/ and switched to SBC6120 disk images from https://www.grc.com. This new image has new benefits 1) "SET TTY NO PAUSE" works which disables the annoying pause every # numbers of lines in TTY output. 2) BASIC works. 
. There are two partitions, SYS: and IDA1:. IDA1: contains games.
