	.TITLE	HELLO WORLD
	.HM6120


	.TITLE	SBC6120 IOTs and Definitions


;   The console terminal interface of the SBC6120 is actually straight -8
; compatible, which is a proper subset of the KL8E except that the KCF, TFL,
; KIE and TSK (or SPI, depending on which manual you read!) instructions are
; omitted.  Console interrupts are permanently enabled, as they were in the
; original PDP-8.  The console interface in the SBC6120 DOES NOT use a 6121,
; so there'll be none of this skip-on-flag-and-clear-it nonsense with KSF or
; TSF!
KSF=6031	; Skip of console receive flag is set
KCC=6032	; Clear receive flag and AC
KRS=6034	; OR AC with receive buffer and DON't clear the flag
KRB=6036	; Read receive buffer into AC and clear the flag
TSF=6041	; Skip if the console transmit flag is set
TCF=6042	; Clear transmit flag, but not the AC
TPC=6044	; Load AC into transmit buffer, but don't clear flag
TLS=6046	; Load AC into transmit buffer and clear the flag
PRISLU=6412	; set console to primary SLU (03/04)

; 8255 PPI Interface IOTs...
PRPA=6470	; read PPI port A
PRPB=6471	;  "	"   "	B
PRPC=6472	;  "	"   "	C
PRCR=6473	;  "	"  control register
PWPA=6474	; write PPI port A
PWPB=6475	;   "	 "    "	 B
PWPC=6476	;   "	 "    "	 C
PWCR=6477	;   "	 "  control register

; Front panel (FP612) IOTs...
CCPR=6430	; clear AC, HALT SW REQ and 30Hz REQ flags
SHSW=6431	; skip on HALT SW REQ flag
SPLK=6432	; skip on panel lock
SCPT=6433	; skip on 30Hz timer REQ flag
RFNS=6434	; read function switches (BOOT, EXAM, DEP, ROTSW, etc)
RLOF=6435	; turn off RUN LED
RLON=6437	;   "  on   "	"
;OSR (7404) read data switches 
;WSR (6246) write data LEDs (when ROTSW != MD)

; Other SBC6120 instructions...
POST=6440	; Display a 4 bit POST code
BPT=PR3		; Breakpoint trap instruction

; Special ASCII control characters that get used here and there...
CHNUL=000	; A null character (for fillers)
CHCTC=003	; Control-C (Abort command)
CHBEL=007	; Control-G (BELL)
CHBSP=010	; Control-H (Backspace)
CHTAB=011	; Control-I (TAB)
CHLFD=012	; Control-J (Line feed)
CHCRT=015	; Control-M (carriage return)
CHCTO=017	; Control-O (Suppress output)
CHXON=021	; Control-Q (XON)
CHCTR=022	; Control-R (Retype command line)
CHXOF=023	; Control-S (XOFF)
CHCTU=025	; Control-U (Delete command line)
CHESC=033	; Control-[ (Escape)
CHDEL=177	; RUBOUT    (Delete)

; OPR microinstructions that load the AC with various special constants...
NL0000=CLA			; all models
NL0001=CLA IAC			; all models
NL0002=CLA CLL CML     RTL	; all models
NL2000=CLA CLL CML     RTR	; all models
NL3777=CLA CLL	   CMA RAR	; all models
NL4000=CLA CLL CML     RAR	; all models
NL5777=CLA CLL	   CMA RTR	; all models
NL7775=CLA CLL	   CMA RTL	; all models
NLM3=NL7775			; all models
NL7776=CLA CLL	   CMA RAL	; all models
NLM2=NL7776			; all models
NL7777=CLA	   CMA		; all models
NLM1=NL7777			; all models
NL0003=CLA STL IAC RAL		; PDP-8/I and later
NL0004=CLA CLL IAC RTL		; PDP-8/I and later
NL0006=CLA STL IAC RTL		; PDP-8/I and later
NL6000=CLA STL IAC RTR		; PDP-8/I and later
NL0100=CLA IAC BSW		; PDP-8/E and later
NL0010=CLA IAC R3L		; HM6120 only


	.FIELD	0
	.PAGE	0

HELLO: 
    NOP				; gets overwritten by 6120 to 07777
HELLO1:
	CLA CLL
 	TAD @STPTR
 	SNA
 	HLT
 	TLS
 	TSF
 	JMP .-1
 	JMP HELLO1

STPTR:	.DATA STRNG-1


STRNG: .DATA 310, 345, 354, 354, 357, 254, 240, 367, 357, 362, 354, 344, 241, 0

	.END